
You are the Captain of your very own spaceship. For this voyage you will need to recruit Officers and Crew, to help you
navigate space. Along the journey you will encounter many obstacles, some dramatic, some common-place. It will be up to
you, your Officers, and Crew to see the Ship to the end of the voyage.

# The Cards

The game consists of three types of cards. Scenario Cards, Officer Cards, and Crew cards. The amount of each will vary
by the size of the Ship you choose.

* Scenario Cards represent the obstacle to be cleared. These may be things like Encountering a Black Hole, or Escaped Exotic Animal on Board.
  * Each Scenario is thematically paired to a Ship Department. See the Ship Sizes section for a description of departments per ship.
* Officer Cards represent the officers assigned to each department. They give the orders that the Crew execute to clear Scenarios.
* Crew Cards represent the men/women/neuters of the crew. These are the hard working sentient beings who initiate and complete the officers orders.
  * Crew cards are split between Initiate Action cards and Complete Action cards.
  

# Setting out the Playing Field

* Separate the Scenario cards and the Officer/Crew cards into two decks.
* Shuffle each deck.
* The Scenario Deck is placed at the top right of the playing field.
  * Deal out four (4) Scenario Cards, face up on the playing field.
  * Leave space beside the Scenario deck for a discard pile.
* The Officer/Crew deck (also known as the Ship Personnel, or SP, deck) is placed at the bottom right of the playing field. These cards should be face down.
  * Leave space beside the SP deck for a discard pile.

# Game Play

#### The goal of the game is to clear all the Scenarios from the playing field. This is done by stacking, in sequence, one Officer card, one Crew Initiate Action card and one Crew Complete Action card.

* Draw either one (1) or three (3) cards from the SP deck.
  * Officer cards can only be played on Scenario cards with matching Ship Departments.
  * Crew Initiate Action cards can only be played on Officer cards with matching Ship Departments.
  * Crew Complete Action cards can only be played on Crew Initiate Action cards with matching Ship Departments.
* If the player has a complete stack of matching Ship Department Scenario, Officer, Crew Initiate Action, 
and Crew Complete Action cards, then the Scenario is considered to be cleared. Remove all cards from the playing field and put them in the 
discard pile. Deal another Scenario card to replace the cleared Scenario.
* If the top card is not playable, place the drawn cards at the bottom of the SP deck and draw the same amount of cards again.
* Continue to draw cards from the SP deck in order to clear Scenarios. When all Scenarios are cleared, the game has been won.
  * If Scenarios remain but cannot be cleared, then the game is over. Re-shuffle and play again! 

