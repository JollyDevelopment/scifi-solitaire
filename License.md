My intent for licencing in this prokect is:

For all media/artwork/images/written content/etc the [Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/) license applies.

For all software code the [GNU Lesser General Public License v3.0 or later](https://choosealicense.com/licenses/lgpl-3.0/) license applies.


 

