#!/usr/bin/env python3


################################################################################
#    Encounters Module
#    Used to create scenarios and populate them from the ship roster.
#    Copyright (C) 2020  Alan Roberts
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
################################################################################

# ----- imports -----
import json
import os
import random
import ast

opj = os.path.join

# ----- variables -----

# ----- functions -----


# get current file location
def get_file_loc():
    # get the current path on disk
    this_loc = os.path.dirname(os.path.abspath(__file__))
    return this_loc


# load all the scenarios as an object
def load_scenario_pool():
    # load the scenarios json
    # check for the env var location first
    # then if that is not set use default loc
    if os.environ.get("ENCOUNTERS_JSON") is not None:
        scen_json = os.environ.get("ENCOUNTERS_JSON")
    else:
        scen_json = opj(get_file_loc(), 'plot/scenarios.json')
    with open(scen_json) as f:
        sj = json.load(f)
    return sj


# load the pronouns as an object
def load_pronouns_pool():
    # load the json
    pronouns_json = opj(get_file_loc(), '../drydock/personnel/pronouns/list.json')
    with open(pronouns_json) as f:
        pj = json.load(f)
    return pj


# load the replacements json
def load_replacements_pool():
    # load the replacements list
    replacements_json = opj(get_file_loc(), '../drydock/personnel/replacements/list.json')
    with open(replacements_json) as f:
        rj = json.load(f)
    return rj


# load the departments file
def load_departments_pool():
    departments_json = opj(get_file_loc(), '../drydock/fleet/assignments/departments.json')
    with open(departments_json, 'r') as f:
        dj = json.load(f)
    return dj


# build the scenario list, returns a dict
def build_scenarios_list(ship_class, departments):
    # load the scenarios.json
    all_scenarios = load_scenario_pool()

    # for the ship_class['scenarios_per] / num_of_departments get that many random scenarios
    # so for the 'frigate' class, that will be: scenarios_per(12) divided by dep_num(3) = 4 random scenarios for
    # each department
    sp = all_scenarios[ship_class]['scenarios_per']
    dn = len(departments)
    scenario_num = (sp / dn)

    # num_list = get_random_nums_to_load(all_scenarios[ship_class], departments, scen_num)

    # get the ship_class scenarios from all_scenarios
    ship = all_scenarios[ship_class]

    # create the dict to hold the full scenario list
    full_scenario_dict = {}

    # print(departments) # testing

    # for each department, pick a random scenario 'scen_num' of times. Make sure the scenarios do not duplicate
    for department in departments:
        # load the scenarios for that dep
        sd = ship[department]
        # print(department) # testing

        # get a list of the keys in the dict
        # while the scenario_list has less items that the scenario_num
        # choose a random key
        # get the index number of that choice
        # pop off that item and add it to the num_list
        scenario_keys = list(sd.keys())
        # print('scenario_keys = ' + str(scenario_keys)) #testing

        dep_scenario_list = []
        while len(dep_scenario_list) < scenario_num:
            c = random.choice(scenario_keys)
            ci = scenario_keys.index(c)
            cp = scenario_keys.pop(ci)
            dep_scenario_list.append(cp)

        # dict to hold the department that is being iterated over's selected scenarios
        department_scenarios = {}

        # add the currently selected scenarios to the department_scenarios
        for scenario_number in dep_scenario_list:
            department_scenarios[scenario_number] = sd[scenario_number]

        # add the department_scenarios to the full_scenario_dict
        full_scenario_dict[department] = department_scenarios

        # reset scenario_keys[] and dep_scenario_list[]
        scenario_keys = []
        dep_scenario_list = []

    return full_scenario_dict
    # return print('done') #testing


# build the filled scenarios list by searching/replacing the pronouns/names
def build_filled_scenarios(raw_scenarios, ship_roster, ship_captain, ship_class):
    # run the replacements
    filled_scenarios = run_replacements(captain=ship_captain, roster=ship_roster, raw_scenarios=raw_scenarios, sclass=ship_class)

    return filled_scenarios


# replace the names/ranks/pronouns of the captain and crew and return the filled out string
def run_replacements(captain=None, roster=None, raw_scenarios=None, sclass=None):
    # copy the raw dict to fill and pass back
    filled_scenarios = raw_scenarios.copy()
    # print(filled_scenarios) #testing

    # load the departments file
    df = load_departments_pool()

    # how many officers per department
    op = df[sclass]['officers_per']

    # how many crew per department
    cp = df[sclass]['crew_per']

    # nested for loops
    # for each key in the raw_scenario.keys() (which are the departments, so for each department)
    # load that key's value ( which will be all the scenarios for that department
    # for each key in that dict (which will be the scenario's number)
    # take the content of the Officer_Order, Crew_Initiate_Action_Description, and Crew_Complete_Action_Description
    # convert that content to a string
    # run replace on it for the officers/captain, and the crew members
    for department in raw_scenarios.keys():
        for scenario in raw_scenarios[department].keys():
            # get the officer string
            officer_order_str = raw_scenarios[department][scenario]['Officer_Order']

            # replace captain name/last name
            officer_order_str = officer_order_str.replace('$CPT_N', captain.full_name)
            officer_order_str = officer_order_str.replace('$CPT_LN', captain.last_name)

            # officer replacements
            x = 1
            while x < (op+1):
                # get the matching departments officer x from the roster
                o = roster[department]['staff'][x]
                ofr = '$O%s_R' % str(x) # build the officers rank string
                ofn = '$O%s_N' % str(x) # build the officers full name string
                ofln = '$O%s_LN' % str(x) # build the officers last name string
                oppesu = '$O%s_P_Pe_Su' % str(x) # build the officers personal subjective pronoun
                oppesuc = '$O%s_P_Pe_Su_C' % str(x) # build the officers capitalized personal subjective pronoun
                oppeob = '$O%s_P_Pe_Ob' % str(x) # build the officers personal objective pronoun
                oppeobc = '$O%s_P_Pe_Ob_C' % str(x) # build the officers capitalized personal objective pronoun
                oppo = '$O%s_P_Po' % str(x) # build the officers possessive pronoun
                oppoc = '$O%s_P_Po_C' % str(x) # build the officers capitalized possessive pronoun
                opre = '$O%s_P_Re' % str(x) # build the officers reflexive pronoun

                # run through the pronoun replacements
                # rank
                officer_order_str = officer_order_str.replace(ofr, o.rank)
                # full name
                officer_order_str = officer_order_str.replace(ofn, o.full_name)
                # last name
                officer_order_str = officer_order_str.replace(ofln, o.last_name)
                # pronouns
                officer_order_str = officer_order_str.replace(oppesuc, o.P_Pe_Su_C())
                officer_order_str = officer_order_str.replace(oppesu, o.P_Pe_Su())
                officer_order_str = officer_order_str.replace(oppeobc, o.P_Pe_Ob_C())
                officer_order_str = officer_order_str.replace(oppeob, o.P_Pe_Ob())
                officer_order_str = officer_order_str.replace(oppoc, o.P_Po_C())
                officer_order_str = officer_order_str.replace(oppo, o.P_Po())
                officer_order_str = officer_order_str.replace(opre, o.P_Re())

                # iterate x
                x = x+1

            # update the filled scenarios dict with the updated string
            filled_scenarios[department][scenario]['Officer_Order'] = officer_order_str

            # get the crew action strings
            crew_ia_desc_str = raw_scenarios[department][scenario]['Crew_Initiate_Action_Description']
            crew_ca_desc_str = raw_scenarios[department][scenario]['Crew_Complete_Action_Description']

            # crew replacements
            y = 1
            while y <= (op+1):
                # get the matching departments crew member y's from the roster
                c = roster[department]['crew'][y]
                cr = '$C0%s_R' % str(y)  # build the crew members rank string
                cn = '$C0%s_N' % str(y)  # build the crew members full name string
                cln = '$C0%s_LN' % str(y)  # build the crew members last name string
                cppesu = '$C0%s_P_Pe_Su' % str(y)  # build the crew members personal subjective pronoun
                cppesuc = '$C0%s_P_Pe_Su_C' % str(y)  # build the crew members capitalized personal subjective pronoun
                cppeob = '$C0%s_P_Pe_Ob' % str(y)  # build the crew members personal objective pronoun
                cppeobc = '$C0%s_P_Pe_Ob_C' % str(y)  # build the crew members capitalized personal objective pronoun
                cppo = '$C0%s_P_Po' % str(y)  # build the crew members possessive pronoun
                cppoc = '$C0%s_P_Po_C' % str(y)  # build the crew members capitalized possessive pronoun
                cpre = '$C0%s_P_Re' % str(y)  # build the crew members reflexive pronoun

                # run through the pronoun replacements
                # rank
                crew_ia_desc_str = crew_ia_desc_str.replace(cr, c.rank)
                crew_ca_desc_str = crew_ca_desc_str.replace(cr, c.rank)
                # full name
                crew_ia_desc_str = crew_ia_desc_str.replace(cn, c.first_name + " " + c.last_name)
                crew_ca_desc_str = crew_ca_desc_str.replace(cn, c.first_name + " " + c.last_name)
                # last name
                crew_ia_desc_str = crew_ia_desc_str.replace(cln, c.last_name)
                crew_ca_desc_str = crew_ca_desc_str.replace(cln, c.last_name)
                # pronouns
                crew_ia_desc_str = crew_ia_desc_str.replace(cppesuc, c.P_Pe_Su_C())
                crew_ia_desc_str = crew_ia_desc_str.replace(cppesu, c.P_Pe_Su())
                crew_ia_desc_str = crew_ia_desc_str.replace(cppeobc, c.P_Pe_Ob_C())
                crew_ia_desc_str = crew_ia_desc_str.replace(cppeob, c.P_Pe_Ob())
                crew_ia_desc_str = crew_ia_desc_str.replace(cppoc, c.P_Po_C())
                crew_ia_desc_str = crew_ia_desc_str.replace(cppo, c.P_Po())
                crew_ia_desc_str = crew_ia_desc_str.replace(cpre, c.P_Re())

                crew_ca_desc_str = crew_ca_desc_str.replace(cppesuc, c.P_Pe_Su_C())
                crew_ca_desc_str = crew_ca_desc_str.replace(cppesu, c.P_Pe_Su())
                crew_ca_desc_str = crew_ca_desc_str.replace(cppeobc, c.P_Pe_Ob_C())
                crew_ca_desc_str = crew_ca_desc_str.replace(cppeob, c.P_Pe_Ob())
                crew_ca_desc_str = crew_ca_desc_str.replace(cppoc, c.P_Po_C())
                crew_ca_desc_str = crew_ca_desc_str.replace(cppo, c.P_Po())
                crew_ca_desc_str = crew_ca_desc_str.replace(cpre, c.P_Re())

                # iterate y
                y = y + 1

            # updated the filled scenarions dict with the updated strings
            filled_scenarios[department][scenario]['Crew_Initiate_Action_Description'] = crew_ia_desc_str
            filled_scenarios[department][scenario]['Crew_Complete_Action_Description'] = crew_ca_desc_str

    return filled_scenarios

# ----- classes -----


class Encounters():
    '''
    This class creates the scenario set for a card batch.

    roster: A json object created by the Ship class that holds the list of Officer and Crew objects

    Returns: scenario_set - a json object that holds the list of scenarios and their descriptions.
    '''
    def __init__(self, roster, ship_class, captain):
        # roster is a dict in the form of
        # {"dept": {"staff": <officer object>}, {"crew": {"0": <crew object>, "1": ...}}}}
        self.roster = roster
        self.ship_class = ship_class
        self.ship_captain = captain

    # get the departments list, by iterating over the keys in the roster_dict. This should be the departments
    @property
    def departments(self):
        deps = []
        for d in self.roster.keys():
            deps.append(d)
        return deps

    # get the numbers of scenarios per department to populate
    @property
    def scenario_num(self):
        scenario_pool = load_scenario_pool()
        dep_num = scenario_pool[self.ship_class]['scenarios_per']
        return dep_num

    # get the raw scenarios list and put it into a new dict
    # pass the list[] of departments, and the number of scenarios to load per department
    @property
    def raw_scenarios_list(self):
        return build_scenarios_list(self.ship_class, self.departments)

    # fill in the search and replace pronouns and names
    def filled_scenarios_list(self):
        return build_filled_scenarios(self.raw_scenarios_list, self.roster, self.ship_captain, self.ship_class)

    # property that holds the finished scenario set
    @property
    def scenario_set(self):
        ss = {}
        return ss

