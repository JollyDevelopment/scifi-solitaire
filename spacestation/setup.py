from distutils.core import setup

setup(
    name='DryDock',
    version='0.1dev',
    packages=['drydock',],
    license='TBD',
    long_description=open('README.txt').read(),
)