#!/usr/bin/env python3

################################################################################
#    Render_Encounters
#    Used for the SciFi-Solitaire game.
#    Creates batches of cards.
#    Copyright (C) 2020 Alan P. Roberts
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
################################################################################

#
# this is a small tool to just make a ship
# then render the scenarios list out with the
# ship roster. 

# its primarily used for grammar checks and such for new encounter additions.


# ----- imports -----
import os
import json
from drydock.fleet.build_a_ship import Ship
from expeditions.encounters import Encounters

# ----- variables -----

# ----- functions -----

# ----- main -----
def main():
    # create a ship
    # print("Building a ship...")
    s = Ship()

    # setup the encounters for the ship
    # print("Setting up the encounters...")
    e = Encounters(s.ship_roster, s.ship_class, s.ship_captain).filled_scenarios_list()

    # print the filled encounters
    print(json.dumps(e))


if __name__ == '__main__':
    main()