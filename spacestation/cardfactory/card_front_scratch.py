#!/usr/bin/env python

################################################################################
#    card_front_scratch
#    Used for the SciFi-Solitaire game.
#    test for card front making
#    Copyright (C) 2020 Alan P. Roberts
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
################################################################################

# ----- imports -----
import os
from pptx import Presentation
from pptx.util import Mm as MM, Pt as PT
from pptx.enum.shapes import MSO_SHAPE
from pptx.enum.text import MSO_ANCHOR, MSO_AUTO_SIZE
from pptx.enum.text import PP_ALIGN
from pptx.dml.color import RGBColor

# ----- variables -----

# ----- functions -----

# ----- classes -----

# ----- main -----
def main():
    # make presentation
    prs = Presentation()

    # set the size
    prs.slide_width = MM(62)
    prs.slide_height = MM(88)

    # blank layout
    lo = 6
    layouts = prs.slide_layouts
    slide_layout = layouts[lo]

    # create slide, get the shapetree
    slide = prs.slides.add_slide(slide_layout)
    shape_tree = slide.shapes

    # add picture
    top = MM(2)
    left = MM(2)
    w = MM(10)
    h = MM(10)

    shape_tree.add_picture('assets/common/officer_order_icon.png', top, left, w, h)

    # save the presentation
    prs.save('picture_test.pptx')








if __name__ == '__main__':
    main()