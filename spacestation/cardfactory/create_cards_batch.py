#!/usr/bin/env python

################################################################################
#    Create_Cards_Batch
#    Used for the SciFi-Solitaire game.
#    Creates batches of cards.
#    Copyright (C) 2020 Alan P. Roberts
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
################################################################################

#
# This creates a set of cards
# it should take as arguments:
# ship_size, ship_name, departments_list
# by default though it will not use those, but let the Ship() object build itself
#
# This ship object is then used to populate the scenarios
# Those scenarios are then parsed out to card sets. (Scenario card, officer card, and two crew cards)

# ----- imports -----
import os
from drydock.fleet.build_a_ship import Ship
from expeditions.encounters import Encounters
from deck.card import Card
from deck.cards import CardDeck
import subprocess as sp

# ----- variables -----

# ----- functions -----


def create_a_deck(encounters, font_name = 'NotoSerif', font_style = '', font_path = 'assets/fonts/NotoSerif-Regular.ttf'):
    # run two for loops, one to go over the departments, then one to make the scenario cards for that department
    for dep in encounters.keys():
        for scen in encounters[dep].keys():
            c = Card(dep, encounters[dep][scen])
            c.add_card_font(font_name, font_style, font_path)
            c.set_card_font(font_name, font_style, 16)
            c.output_card_pdf('output/%s_%s.pdf' % (dep, scen))
            # print(encounters[dep][scen])


def main():
    # create a ship
    print("Building a ship...")
    s = Ship()

    # setup the encounters for the ship
    print("Setting up the encounters...")
    e = Encounters(s.ship_roster, s.ship_class, s.ship_captain).filled_scenarios_list()

    # create a deck
    print("Creating a deck...")
    template = 'assets/pptx/card_template_2.pptx'
    output = 'output/deck.pptx'
    d = CardDeck(e, output)
    d.create_cards()

    # convert pptx to pdf
    print("Converting deck to PDF format...")
    mac_lo = '/Applications/LibreOffice.app/Contents/MacOS/soffice'
    sp.call([mac_lo, '--headless', '--convert-to', 'pdf', output, '--outdir', 'output'])


if __name__ == '__main__':
    main()