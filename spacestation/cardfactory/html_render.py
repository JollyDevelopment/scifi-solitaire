#!/usr/bin/env python

################################################################################
#    html_render
#    Used for the SciFi-Solitaire game.
#    Creates batches of cards.
#    Copyright (C) 2020 Alan P. Roberts
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
################################################################################

# ----- imports -----
from selenium.webdriver import Firefox, FirefoxOptions
from selenium.webdriver import Chrome, ChromeOptions

# ----- variables -----


# ----- functions -----


# ----- classes -----


# ----- main -----

def main():
    # opts = FirefoxOptions()
    # opts.add_argument("--width=1920")
    # opts.add_argument("--height=1080")
    #
    # driver = Firefox(options=opts)
    # driver = Firefox()
    # driver.get('https://python.org')
    # driver.save_screenshot("screenshot.png")

    driver = Chrome(executable_path='/Users/aroberts/GIT_Repos/scifi-solitaire/overhead/chrome_driver/chromedriver')
    width = 800
    height = 600
    driver.set_window_size(width=width,height=height)
    url = 'http://beacon.io'
    html_file = 'file:///Users/aroberts/GIT_Repos/scifi-solitaire/spacestation/cardfactory/assets/html/card_format.html'
    driver.get(html_file)
    driver.save_screenshot("screenshot.png")




if __name__ == '__main__':
    main()