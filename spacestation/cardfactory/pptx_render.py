#!/usr/bin/env python

################################################################################
#    pptx_render
#    Used for the SciFi-Solitaire game.
#    Creates batches of cards.
#    Copyright (C) 2020 Alan P. Roberts
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
################################################################################

# ----- imports -----
from pptx import Presentation
from pptx.util import Mm as MM, Pt as PT
from pptx.enum.shapes import MSO_SHAPE
from pptx.enum.text import MSO_ANCHOR, MSO_AUTO_SIZE
from pptx.enum.text import PP_ALIGN
from pptx.dml.color import RGBColor


# ----- variables -----
SLD_LAYOUT_TITLE_AND_CONTENT = 1
scenario = {
    "Title": "Distress Call - Pirates!",
    "Description": "In the depths of the cold void, any cry for help must be answered! The honor of a ships crew demands that they come to the aid of any in need. Beware however for not all ships feel the same way...",
    "Officer_Order": "$O1_R $O1_LN reports a new signal. Its a distress call! We have to answer, but be cautious, it could be a trick!",
    "Crew_Initiate_Action_Description": "$C01_R $C01_N examines the data and ship profiles. $C01_P_Pe_Su tweaks the long range sensors for maximum gain.",
    "Crew_Complete_Action_Description": "$C02_R spots a second stealth ship hiding behind the “distressed merchant”, in just enough time for the pilot to reroute around the pirates maximum range and get the ship to safety."
}

# ----- functions -----


# ----- classes -----


# ----- main -----

def main():
    # open the template
    prs = Presentation('assets/pptx/card_template.pptx')
    # prs = Presentation()
    # set the layout (blank)
    blank = 0
    slide_layouts = prs.slide_layouts
    slayout = slide_layouts[blank]

    # for loop to create the cards
    # create slide, get the shapetree
    slide = prs.slides.add_slide(slayout)
    shape_tree = slide.shapes

    # create title box
    # origin point
    top = MM(2.5)
    left = MM(2.5)
    # width and height
    w = MM(57)
    h = MM(10)
    title_box = shape_tree.add_shape(
        MSO_SHAPE.RECTANGLE, left, top, w, h
    )
    # turn off shadow inheritance
    effectRef = title_box._element.xpath('.//p:style/a:effectRef')[0]
    effectRef.set('idx', '0')
    # set the box to transparent
    title_box.fill.background()
    title_box.line.fill.background()
    # get the text_frame, set the vertical alignment, set auto-fit on the text
    tb_tf = title_box.text_frame
    tb_tf.vertical_anchor = MSO_ANCHOR.MIDDLE
    tb_tf.auto_size = MSO_AUTO_SIZE.TEXT_TO_FIT_SHAPE
    # get the first paragraph, set the horizontal alignment
    tb_para = tb_tf.paragraphs[0]
    tb_para.alignment = PP_ALIGN.CENTER
    # make a run, , set the text, set the font color
    tb_run = tb_para.add_run()
    tb_run.text = scenario['Title']
    tb_font = tb_run.font
    tb_font.color.rgb = RGBColor(0,0,0)

    # create the officer order box
    # origin point
    top2 = MM(15)
    h2 = MM(15)
    oo_box = shape_tree.add_shape(
        MSO_SHAPE.RECTANGLE, left, top2, w, h2
    )
    # turn off shadow inheritance
    effectRef = oo_box._element.xpath('.//p:style/a:effectRef')[0]
    effectRef.set('idx', '0')
    # set the box to transparent
    oo_box.fill.background()
    oo_box.line.fill.background()
    # get the text_frame, set the vertical alignment, set auto-fit on the text
    oo_box_tf = oo_box.text_frame
    oo_box_tf.vertical_anchor = MSO_ANCHOR.MIDDLE
    oo_box_tf.auto_size = MSO_AUTO_SIZE.TEXT_TO_FIT_SHAPE
    # get the first paragraph, set the horizontal alignment
    oo_box_para = oo_box_tf.paragraphs[0]
    oo_box_para.alignment = PP_ALIGN.LEFT
    # make a run, , set the text, set the font color
    oo_run = oo_box_para.add_run()
    oo_run.text = scenario['Officer_Order']
    oo_font = oo_run.font
    oo_font.color.rgb = RGBColor(0,0,0)

    # create the crew IA box
    # origin point
    top3 = MM(32.5)
    h3 = MM(25)
    cia_box = shape_tree.add_shape(
        MSO_SHAPE.RECTANGLE, left, top3, w, h3
    )
    # turn off shadow inheritance
    effectRef = cia_box._element.xpath('.//p:style/a:effectRef')[0]
    effectRef.set('idx', '0')
    # set the box to transparent
    cia_box.fill.background()
    cia_box.line.fill.background()
    # get the text_frame, set the vertical alignment, set auto-fit on the text
    cia_tf = cia_box.text_frame
    cia_tf.vertical_anchor = MSO_ANCHOR.MIDDLE
    cia_tf.auto_size = MSO_AUTO_SIZE.TEXT_TO_FIT_SHAPE
    # get the first paragraph, set the horizontal alignment
    cia_para = cia_tf.paragraphs[0]
    cia_para.alignment = PP_ALIGN.LEFT
    # make a run, , set the text, set the font color
    cia_run = cia_para.add_run()
    cia_run.text = scenario['Crew_Initiate_Action_Description']
    cia_font = cia_run.font
    cia_font.color.rgb = RGBColor(0,0,0)

    # create the crew CA box
    # origin point
    top4 = MM(60)
    h4 = MM(25)
    cca_box = shape_tree.add_shape(
        MSO_SHAPE.RECTANGLE, left, top4, w, h4
    )
    # turn off shadow inheritance
    effectRef = cca_box._element.xpath('.//p:style/a:effectRef')[0]
    effectRef.set('idx', '0')
    # set the box to transparent
    cca_box.fill.background()
    cca_box.line.fill.background()
    # get the text_frame, set the vertical alignment, set auto-fit on the text
    cca_tf = cca_box.text_frame
    cca_tf.vertical_anchor = MSO_ANCHOR.MIDDLE
    cca_tf.auto_size = MSO_AUTO_SIZE.TEXT_TO_FIT_SHAPE
    # get the first paragraph, set the horizontal alignment
    cca_para = cca_tf.paragraphs[0]
    cca_para.alignment = PP_ALIGN.LEFT
    # make a run, , set the text, set the font color
    cca_run = cca_para.add_run()
    cca_run.text = scenario['Crew_Complete_Action_Description']
    cca_font = cca_run.font
    cca_font.color.rgb = RGBColor(0,0,0)




      # create crew IA
      # create crew CA
    # save the pptx
    prs.save('output/tmp_card.pptx')





if __name__ == '__main__':
    main()