#!/usr/bin/env python

################################################################################
#    Cards
#    Used for the SciFi-Solitaire game.
#    Creates batches of cards.
#    Copyright (C) 2020 Alan P. Roberts
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
################################################################################

# ----- imports -----
from pptx import Presentation
from pptx.util import Mm as MM, Pt as PT
from pptx.enum.shapes import MSO_SHAPE
from pptx.enum.text import MSO_ANCHOR, MSO_AUTO_SIZE
from pptx.enum.text import PP_ALIGN
from pptx.dml.color import RGBColor

# ----- variables -----

# ----- functions -----

# ----- classes -----

class CardDeck(object):
    def __init__(self, encounters, output_path, template_path=None, slide_width=None, slide_height=None):
        if template_path:
            self.template = template_path
        self.slide_width = slide_width or MM(62)
        self.slide_height = slide_height or MM(88)
        self.output = output_path
        self.encounters = encounters
        self.prs = Presentation(template_path) or Presentation()
        self.slide_layout = self.set_layout()

    def set_layout(self):
        blank = 6 # this should be the blank layout in the python-pptx template file
        slide_layouts = self.prs.slide_layouts
        return slide_layouts[blank]

    def testfn(self):
        print(type(self.encounters))
        for dep in self.encounters.keys():
            for scen in self.encounters[dep].keys():
                print(self.encounters[dep][scen])


    def create_cards(self):
        # set the card width and height
        self.prs.slide_width = self.slide_width
        self.prs.slide_height = self.slide_height

        # remove the master slide's placeholders
        # get the masterslide_shapetree
        # ms_phs = self.prs.slide_master.placeholders
        # iterate over the tree (of placeholders), grab the .element, remove the element/placeholder from the parent
        # shapetree. Count down so we are removing from the the 'last' placeholder. That preserves the number sequence.
        # If we remove the 0 item first, then the rest of them change their numbers and we get a indexerror
        # end = len(ms_phs)
        # for i in range((end - 1), -1, -1):
        #     mph = ms_phs[i]
        #     mph_e = mph.element
        #     mph_e.getparent().remove(mph_e)
        # self.prs.slide_master.placeholders = ms_phs

        # run two for loops, one to go over the departments, then one to make the scenario cards for that department
        for dep in self.encounters.keys():
            print(dep)
            for scen in self.encounters[dep].keys():
                # create slide, get the shapetree
                slide = self.prs.slides.add_slide(self.slide_layout)
                shape_tree = slide.shapes

                # create the department box
                #origin point
                top = MM(1)
                left = MM(2.5)
                # width and height
                w = MM(57.5)
                h = MM(3)
                department_box = shape_tree.add_shape(
                    MSO_SHAPE.RECTANGLE, left, top, w, h
                )
                # turn off shadow inheritance
                effectRef = department_box._element.xpath('.//p:style/a:effectRef')[0]
                effectRef.set('idx', '0')
                # set the box to transparent
                department_box.fill.background()
                department_box.line.fill.background()
                # get the text_frame, set the vertical alignment, set auto-fit on the text
                db_tf = department_box.text_frame
                db_tf.vertical_anchor = MSO_ANCHOR.MIDDLE
                # db_tf.auto_size = MSO_AUTO_SIZE.TEXT_TO_FIT_SHAPE
                # get the first paragraph, set the horizontal alignment
                db_para = db_tf.paragraphs[0]
                db_para.alignment = PP_ALIGN.CENTER
                # make a run, , set the text, set the font color
                db_run = db_para.add_run()
                db_run.text = '--- ' + dep.capitalize() + ' Scenario ---'
                db_font = db_run.font
                db_font.size = PT(5)
                db_font.color.rgb = RGBColor(0, 0, 0)


                # create title box
                # origin point
                top = MM(6)
                left = MM(2.5)
                # width and height
                w = MM(57)
                h = MM(10)
                title_box = shape_tree.add_shape(
                    MSO_SHAPE.RECTANGLE, left, top, w, h
                )
                # turn off shadow inheritance
                effectRef = title_box._element.xpath('.//p:style/a:effectRef')[0]
                effectRef.set('idx', '0')
                # set the box to transparent
                title_box.fill.background()
                title_box.line.fill.background()
                # get the text_frame, set the vertical alignment, set auto-fit on the text
                tb_tf = title_box.text_frame
                tb_tf.vertical_anchor = MSO_ANCHOR.MIDDLE
                tb_tf.auto_size = MSO_AUTO_SIZE.TEXT_TO_FIT_SHAPE
                # get the first paragraph, set the horizontal alignment
                tb_para = tb_tf.paragraphs[0]
                tb_para.alignment = PP_ALIGN.CENTER
                # make a run, , set the text, set the font color
                tb_run = tb_para.add_run()
                tb_run.text = self.encounters[dep][scen]['Title']
                tb_font = tb_run.font
                tb_font.color.rgb = RGBColor(0, 0, 0)

                # create the officer order box
                # origin point
                top2 = MM(18)
                h2 = MM(17)
                oo_box = shape_tree.add_shape(
                    MSO_SHAPE.RECTANGLE, left, top2, w, h2
                )
                # turn off shadow inheritance
                effectRef = oo_box._element.xpath('.//p:style/a:effectRef')[0]
                effectRef.set('idx', '0')
                # set the box to transparent
                oo_box.fill.background()
                oo_box.line.fill.background()
                # get the text_frame, set the vertical alignment, set auto-fit on the text
                oo_box_tf = oo_box.text_frame
                oo_box_tf.vertical_anchor = MSO_ANCHOR.MIDDLE
                oo_box_tf.auto_size = MSO_AUTO_SIZE.TEXT_TO_FIT_SHAPE
                # get the first paragraph, set the horizontal alignment
                oo_box_para = oo_box_tf.paragraphs[0]
                oo_box_para.alignment = PP_ALIGN.LEFT
                # make a run, , set the text, set the font color
                oo_run = oo_box_para.add_run()
                oo_run.text = self.encounters[dep][scen]['Officer_Order']
                oo_font = oo_run.font
                oo_font.color.rgb = RGBColor(0, 0, 0)

                # create the crew IA box
                # origin point
                top3 = MM(37.5)
                h3 = MM(22)
                cia_box = shape_tree.add_shape(
                    MSO_SHAPE.RECTANGLE, left, top3, w, h3
                )
                # turn off shadow inheritance
                effectRef = cia_box._element.xpath('.//p:style/a:effectRef')[0]
                effectRef.set('idx', '0')
                # set the box to transparent
                cia_box.fill.background()
                cia_box.line.fill.background()
                # get the text_frame, set the vertical alignment, set auto-fit on the text
                cia_tf = cia_box.text_frame
                cia_tf.vertical_anchor = MSO_ANCHOR.MIDDLE
                cia_tf.auto_size = MSO_AUTO_SIZE.TEXT_TO_FIT_SHAPE
                # get the first paragraph, set the horizontal alignment
                cia_para = cia_tf.paragraphs[0]
                cia_para.alignment = PP_ALIGN.LEFT
                # make a run, , set the text, set the font color
                cia_run = cia_para.add_run()
                cia_run.text = self.encounters[dep][scen]['Crew_Initiate_Action_Description']
                cia_font = cia_run.font
                cia_font.color.rgb = RGBColor(0, 0, 0)

                # create the crew CA box
                # origin point
                top4 = MM(63)
                h4 = MM(22)
                cca_box = shape_tree.add_shape(
                    MSO_SHAPE.RECTANGLE, left, top4, w, h4
                )
                # turn off shadow inheritance
                effectRef = cca_box._element.xpath('.//p:style/a:effectRef')[0]
                effectRef.set('idx', '0')
                # set the box to transparent
                cca_box.fill.background()
                cca_box.line.fill.background()
                # get the text_frame, set the vertical alignment, set auto-fit on the text
                cca_tf = cca_box.text_frame
                cca_tf.vertical_anchor = MSO_ANCHOR.MIDDLE
                cca_tf.auto_size = MSO_AUTO_SIZE.TEXT_TO_FIT_SHAPE
                # get the first paragraph, set the horizontal alignment
                cca_para = cca_tf.paragraphs[0]
                cca_para.alignment = PP_ALIGN.LEFT
                # make a run, , set the text, set the font color
                cca_run = cca_para.add_run()
                cca_run.text = self.encounters[dep][scen]['Crew_Complete_Action_Description']
                cca_font = cca_run.font
                cca_font.color.rgb = RGBColor(0, 0, 0)

        self.prs.save(self.output)



