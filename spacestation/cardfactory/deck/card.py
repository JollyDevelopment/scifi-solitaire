#!/usr/bin/env python

################################################################################
#    Card
#    Used for the SciFi-Solitaire game.
#    Base Card Object.
#    Copyright (C) 2020 Alan P. Roberts
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
################################################################################

# imports
from fpdf import FPDF

# variables

# classes


# Card object, will be fed a department and a scenario and then have the .output function create the pdf
# functions: add_font, set_font, output_pdf
class Card(object):
    def __init__(self, department, scenario):
        self.department = department
        self.scenario = scenario
        self.pdf = FPDF('P','mm',(62,88))

    # add a font
    def add_card_font(self, name, style, path):
        self.pdf.add_font(name, style=style, fname=path, uni=True)

    # set the font
    def set_card_font(self, name, style, size):
        self.pdf.set_font(name, style, size)

    # create the card pdf
    def build_card(self):
        self.pdf.set_margins(2, 2, 2)
        self.pdf.add_page()
        self.pdf.set_auto_page_break(True, 2)
        self.pdf.set_font_size(6)
        self.pdf.cell(0, 4, self.department, 0, 1, 'C')
        self.pdf.set_font_size(10)
        self.pdf.cell(0, 6, self.scenario['Title'], 1, 1, 'C')
        self.pdf.cell(0, 4, '', 0, 1)  # 4mm spacer
        self.pdf.set_font_size(8)
        self.pdf.multi_cell(0, 4, self.scenario['Officer_Order'], 0, 'L')
        self.pdf.cell(0, 4, '', 0, 1)  # 4mm spacer
        self.pdf.multi_cell(0, 4, self.scenario['Crew_Initiate_Action_Description'], 0, 'L')
        self.pdf.cell(0, 4, '', 0, 1)  # 4mm spacer
        self.pdf.multi_cell(0, 4, self.scenario['Crew_Complete_Action_Description'], 0, 'L')

    # output the pdf
    def output_card_pdf(self, path):
        self.build_card()
        self.pdf.output(path, 'F')
