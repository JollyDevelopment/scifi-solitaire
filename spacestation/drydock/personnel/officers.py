#!/usr/bin/env python

################################################################################
#    DryDock Module
#    Used for creating ship/officer/crew for the SciFi-Solitaire game.
#    Copyright (C) 2020 Alan P. Roberts
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
################################################################################

#
# This is the Officers class file.
#

# -----imports-----
import json
import random
import os

opj = os.path.join

# -----functions-----


def assign_rank(is_captain):
    # grab the ranks and load them into a dict
    this_loc = os.path.dirname(os.path.abspath(__file__))
    oranks = opj(this_loc, 'ranks/officer_ranks.json')
    with open(oranks, 'r') as f:
        ranks = json.load(f)

    if is_captain:
        return ranks['5']
    else:
        return ranks[str((random.randint(1, 4)))]


def decide_sex():
    sx = random.randint(1,3)
    if sx == 1:
        return "F"
    if sx == 2:
        return "M"
    else:
        return "N"


def get_pronouns(sex):
    # grab the pronouns list, load it from the json
    this_loc = os.path.dirname(os.path.abspath(__file__))
    pl = opj(this_loc, 'pronouns/list.json')
    with open(pl, 'r') as f:
        plj = json.load(f)

    # return the chosen sex's pronouns
    return plj[sex]


def get_first_name(sex):
    # grab the male/female/neuter names list
    this_loc = os.path.dirname(os.path.abspath(__file__))
    if sex == "F":
        fnames = opj(this_loc, 'onomastics/first_names_female.json')
        with open(fnames, 'r') as f:
            names = json.load(f)
    elif sex == "M":
        mnames = opj(this_loc, 'onomastics/first_names_male.json')
        with open(mnames, 'r') as f:
            names = json.load(f)
    else:
        nnames = opj(this_loc, 'onomastics/first_names_neuter.json')
        with open(nnames, 'r') as f:
            names = json.load(f)

    first_name = random.choice(names)
    return first_name


def get_last_name():
    # get the last name from the last names list
    this_loc = os.path.dirname(os.path.abspath(__file__))
    lnames = opj(this_loc, 'onomastics/last_names.json')
    with open(lnames, 'r') as f:
        names = json.load(f)
    last_name = random.choice(names)
    return str(last_name).lower().capitalize()


# -----classes-----


class Officer(object):
    def __init__(self, is_captain=False):
        self.is_cpt = is_captain
        self.rank = assign_rank(self.is_cpt)
        self.sex = decide_sex()
        self.pronouns = get_pronouns(self.sex)
        self.first_name = get_first_name(self.sex)
        self.last_name = get_last_name()
        self.full_name = "%s %s" % (self.first_name, self.last_name)
        self.department = ""
        self.officer_number = ""

    def assignment(self, dept):
        self.department = dept

    # this officers pronouns
    def P_Pe_Su(self):
        return self.pronouns['personal']['subjective']

    def P_Pe_Su_C(self):
        return self.pronouns['personal']['subjective_capitalized']

    def P_Pe_Ob(self):
        return self.pronouns['personal']['objective']

    def P_Pe_Ob_C(self):
        return self.pronouns['personal']['objective_capitalized']

    def P_Po(self):
        return self.pronouns['possessive']

    def P_Po_C(self):
        return self.pronouns['possessive_capitalized']

    def P_Re(self):
        return self.pronouns['reflexive']


