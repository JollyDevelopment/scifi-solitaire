# scifi-solitaire

NAMES-F.TXT and NAMES-M.TXT come from the [Moby Words List](https://www.gutenberg.org/ebooks/3201)

NAMES-L.TXT is pulled from the [1990 Census Data for the US](https://www.census.gov/topics/population/genealogy/data/1990_census/1990_census_namefiles.html)