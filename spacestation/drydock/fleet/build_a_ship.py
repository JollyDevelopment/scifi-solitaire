#!/usr/bin/env python

################################################################################
#    DryDock Module
#    Used for creating ship/officer/crew for the SciFi-Solitaire game.
#    Copyright (C) 2020 Alan P. Roberts
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
################################################################################

#
# This is the Frigate class file.
#

import json
import os
from drydock.personnel.crew import Crew
from drydock.personnel.officers import Officer

opj = os.path.join


# -----functions------

def recruit_captain():
    CPT = Officer(is_captain=True)
    return CPT


def recruit_crew(ship_class):
    # create crew roster, dependent on ship size and department number
    # this returns nested dicts. See assets/XXX_shipt_roster_example.json for an
    # exploded view of what it should look like

    # The number of departments and crew per department are based on ship_size
    # and found in the personnel.assignments.departments.json file.

    # get the current path on disk
    this_loc = os.path.dirname(os.path.abspath(__file__))

    # open the departments file, pull the info about the ship's organization by ship size
    dfile = opj(this_loc, 'assignments/departments.json')
    org = {}
    with open(dfile) as f:
        fleet_deps = json.load(f)
        org = fleet_deps[ship_class]

    ships_roster ={}
    for dept in org["dep_names"]:
        # create a dict of staff officers, ala {1: <officer_obj>, 2: <officer_obj>}
        # the number of staff is dictated by the int in "officers_per"
        staff = {}
        for o in range(1, (org["officers_per"]+1)):
            Off = Officer()
            Off.assignment(dept)
            staff[o] = Off

        # create a dict of crew. Similar to the staff, but the first crew (1) needs to be
        # a non-comm, then start at '1' and make crew up the the number of crew per department
        crew = {1: Crew(non_comm=True)}
        for c in range(2, (org["crew_per"]+1)):
            crew[c] = Crew()

        # assign both staff and crew dicts to dept_roster dict
        dept_roster = {"staff": staff, "crew": crew}

        # assign dept roster to ship roster
        ships_roster[dept] = dept_roster

    return ships_roster

# -----classes-----


class Ship(object):
    def __init__(self, name="The Ship", ship_class="frigate"):
        self.name = name
        self.ship_class = ship_class
        self.ship_captain = recruit_captain()
        self.ship_roster = recruit_crew(self.ship_class)

