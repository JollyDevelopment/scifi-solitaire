extends PopupPanel


# ----- variables -----
var encounter = {}
var test_encounter = {"Title": "Distress Call - Pirates!", "Description": "In the depths of the cold void, any cry for help must be answered! The honor of a ships crew demands that they come to the aid of any in need. Beware however for not all ships feel the same way...", "Officer_Order": "$O1_R $O1_LN reports a new signal. Its a distress call! We have to answer, but be cautious, it could be a trick!", "Crew_Initiate_Action_Description": "$C01_R $C01_N examines the data and ship profiles. $C01_P_Pe_Su_C tweaks the long range sensors for maximum gain.", "Crew_Complete_Action_Description": "$C02_R $C02_N spots a second stealth ship hiding behind the “distressed merchant”, in just enough time for the pilot to reroute around the pirates maximum range and get the ship to safety." }
@onready var etitle = get_node("MC/HBC/ScrollContainer/VBC/Title")
@onready var oo = get_node("MC/HBC/ScrollContainer/VBC/OfficerOrder")
@onready var cia = get_node("MC/HBC/ScrollContainer/VBC/CIA")
@onready var cca = get_node("MC/HBC/ScrollContainer/VBC/CCA")
#@onready var bg_img = get_node("BG_IMG")
var i_x
var i_y

# --- instance references
var playfield_instance

# ----- signals -----
signal popup_closed

# ----- my functions -----

func get_dimensions():
	# get window dimensions of our 'parent'
	# ie the playfiend
	i_x = playfield_instance.get_window().get_size().x
	i_y = playfield_instance.get_window().get_size().y
	
func build_popup_rect():
	get_dimensions()
	var x_5 = i_x * .05 # 5%
	var x_10 = i_x * .10 # 10%
	var x_90 = i_x * .90 # 90%
	var x_95 = i_x * .95 # 95%
	
	var y_5 = i_y * .05 # 05%
	var y_10 = i_y * .10 # 10%
	var y_15 = i_y * .15 # 15%
	var y_25 = i_y * .25 # 25%
	var y_30 = i_y * .30 # 30%
	var y_45 = i_y * .45 # 45%
	var y_60 = i_y * .60 # 60%
	var y_75 = i_y * .75 # 75%
	var y_90 = i_y * .90 # 90%
	
	# make a rect2(x pos, y pos, x width, y height)
	var pop_width = (i_x - x_10)
	var pop_height = (i_y - y_45)

	var box = Rect2(x_5, y_15, pop_width, pop_height)
	return box

func add_encounter_dict(e):
#	self.set_title(e["Title"])
	etitle.text = e["Title"]
	oo.text = e["Officer_Order"]
	cia.text = e["Crew_Initiate_Action_Description"]
	cca.text = e["Crew_Complete_Action_Description"]
	
func show_encounter_popup():
	Global.a_story_mode_popup_is_open = true
	popup(build_popup_rect())

func _react_to_popup_closing():
	Global.a_story_mode_popup_is_open = false
	emit_signal("popup_closed")

func connect_popup_closed_signal():
	if playfield_instance:
		connect("popup_closed", Callable(playfield_instance, "check_for_win_condition"))
			
func disconnect_popup_closed_signal():
	if playfield_instance:
		disconnect("popup_closed", Callable(playfield_instance, "check_for_win_condition"))

# ------ built in functions ------

func _exit_tree():
	disconnect_popup_closed_signal()

# ----- ready ------


# Called when the node enters the scene tree for the first time.
func _ready():
	connect_popup_closed_signal()



