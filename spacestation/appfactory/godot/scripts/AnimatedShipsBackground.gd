extends Node2D

# ----- variables -----
var i_x
var i_y
var orig_x
@onready var orig_y = get_window().get_size().y
@onready var rng = RandomNumberGenerator.new()

# --- constants
const Ship = preload("res://scenes/Spaceship.tscn")


# ----- my functions -----

func set_dimensions():
	# get window dimensions
	i_x = get_window().get_size().x
	i_y = get_window().get_size().y

func adjust_ship_spawn_border():
	# get the border curve2d
	var bc = $Border.get_curve()
	# get a random float between 5.0 and 6.0
	rng.randomize()
	var new_interval = rng.randf_range(5.0,6.0)
	# set the curve bake_interval to that random float
	bc.set_bake_interval(new_interval)
	# call get_baked_length to force the points to be re-calculated and re-cached
	var _bl = bc.get_baked_length()

func set_shiptimer_timeout(time):
	$ShipTimer.set_wait_time(time)

# ----- built in functions -----

func _on_viewport_size_changed():
	set_dimensions()
	await get_tree().process_frame
	$Border.position.y = i_y
	$Border/ShipSpawnLine.position.y = i_y
	adjust_ship_spawn_border()
	var new_baked_points = $Border.get_curve().get_baked_points()
	
	var y_diff = i_y - orig_y

	for i in range(new_baked_points.size()):
		var p = new_baked_points[i]
		new_baked_points.set(i,Vector2(p.x,(p.y + y_diff)))


func _on_ShipTimer_timeout():
	var ship_spawn_location = get_node("Border/ShipSpawnLine")
	ship_spawn_location.progress = randi()
	
	var ship = Ship.instantiate()
	
	# Set the mob's direction perpendicular to the path direction.
	# PI is a built in in gdscript for pi ie 3.14159265358979
	var direction = (ship_spawn_location.rotation + (PI / 2) + (PI / 4))
	
	# Set the mob's position to a random location.
	ship.position = ship_spawn_location.position
	
	ship.set_ship_angle(45)

	# Choose the velocity.
	var velocity = Vector2(randf_range(ship.min_speed, ship.max_speed), 0)
	ship.linear_velocity = velocity.rotated(direction)
	
	rng.randomize()
	var shipscale = rng.randf_range(.01,.10)
	ship.set_ship_scale(Vector2(shipscale,shipscale))
	
	$Traffic.add_child(ship)
	
	
# ----- ready -----

# Called when the node enters the scene tree for the first time.
func _ready():
	set_dimensions()
	randomize()
#	set_shiptimer_timeout(.05)
	$ShipTimer.start()

	# get the viewport size changed signal and 
	# connect the local size_changed function
	# warning-ignore:return_value_discarded
	get_tree().root.connect("size_changed", Callable(self, "_on_viewport_size_changed"))
