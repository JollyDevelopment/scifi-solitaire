extends TextureRect


# ----- variables -----
var encounter_stacks_array = []
var mouse_in = false
var play_field
const one = preload("res://assets/decks/pngs/deck_one_234x332_px.png")
const two = preload("res://assets/decks/pngs/deck_two_234x332_px.png")
const three = preload("res://assets/decks/pngs/deck_three_234x332_px.png")
const empty = preload("res://assets/decks/pngs/deck_empty_234x332_px.png")


# ----- my functions -----

# sets the image texture based on the number of cards in the 
# card_array
func set_image_texture():
	var c = encounter_stacks_array.size()
	if c == 0:
		set_texture(empty)
	elif c == 1:
		set_texture(one)
	elif c == 2:
		set_texture(two)
	else:
		set_texture(three)

# sets the scaling for the instance
# takes a Vector2
func set_ed_scale(vec):
	await get_tree().process_frame
	scale = vec

func add_initial_encounter_stacks(encounterstackarray):
	encounter_stacks_array = encounterstackarray
	
# draw_encounter
# this will be called when the deck is pressed/clicked 
# it returns one EncounterStack reference
func initial_encounter():
	# pop the first encounterstack off the array
	# return the reference
	return encounter_stacks_array.pop_front()

func draw_encounter():
	if encounter_stacks_array.size() > 0:
		# only continue if there is an open EncounterStack
		if play_field.check_for_empty_encounter_stacks():
			# pop off the first encounter and add it to the 
			# pending array
			var es = encounter_stacks_array.pop_front()
			play_field.add_encounterstack_to_pending(es)
			
			# populate the stacks from pending
			play_field.populate_encounters()
	else:
		print("no more encounters!")
	
func reset_encounters_deck():
	while encounter_stacks_array.size() > 0:
		var es = encounter_stacks_array.pop_front()
		es.queue_free()
		
func report_remaining_encounters():
	return encounter_stacks_array.size()
	
# --- debug functions

func report_encounters():
	print("encounters deck size: ", encounter_stacks_array.size())
	print("encounters deck: ", encounter_stacks_array)
	
func poll_encounters():
	for e in encounter_stacks_array:
		print(e.name)
	
	
# ----- input event functions -----

func _on_EncountersDeck_mouse_entered():
	mouse_in = true

func _on_EncountersDeck_mouse_exited():
	mouse_in = false

func _on_EncountersDeck_gui_input(event):
	if Input.is_action_pressed("leftclick") and mouse_in:
		draw_encounter()
	if event is InputEventScreenTouch:
		if event.is_pressed():
			draw_encounter()


# ----- builtin functions ------

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	set_image_texture()

# ----- ready -----

# Called when the node enters the scene tree for the first time.
func _ready():
	set_image_texture()






