extends Control


# ----- variables -----
var total_imgs = 5
@onready var img_grid = get_node("CL/HBC/VBC/TextContainer2/VBC/HBC/SC/ImgGrid")
const Thumbnail = preload("res://scenes/CardThemeThumbButton.tscn")
var back_mouse_in = false
@onready var bg_img = get_node("Background/Img")

# ----- my functions -----

func set_background_img():
	var img_path = str("res://assets/background/full/",Global.bg_num,".png")
	bg_img.set_texture(load(img_path))
	bg_img.position = Global.mobile_bg_position
	Global.set_bg_img_scale(get_window().get_size().y)
#	bg_img.scale = Global.bg_scale
	bg_img.scale = Global.mobile_bg_scale
	bg_img.centered = false

func load_currently_set_selection():
	Global.load_settings()
	for c in img_grid.get_children():
		if c.name == Global.card_color_set:
			c.set_as_selected()
			
func add_thumbnail_buttons_to_grid():
	var x = 0
	while x < total_imgs:
#		print(x)
		var tb = Thumbnail.instantiate()
		tb.set_name(str(x))
		tb.card_theme_selector_page = self
		img_grid.add_child(tb)
		tb.set_img(x)
#		tb.rect_scale = Vector2(.85,.85)
		tb.set_img_scale_percent(.65)
#		tb.set_img_scale()
		x += 1

func _set_card_theme_selection(number):
	Global.card_color_set = number
	turn_other_selections_off(number)
#	set_background_img()
	Global.save_settings()
	
func turn_other_selections_off(number):
	for c in img_grid.get_children():
		if c.name == number:
			continue
		else:
			c.unselect_card_theme()
	
	
# ----- built in functions -----

func _on_Back_mouse_entered():
	back_mouse_in = true

func _on_Back_mouse_exited():
	back_mouse_in = false

func _on_Back_gui_input(event):
	if Input.is_action_just_pressed("leftclick") and back_mouse_in:
		Global.goto_scene("res://scenes/SettingsPage.tscn")
	if event is InputEventScreenTouch:
		if event.is_pressed():
			Global.goto_scene("res://scenes/MobileSettingsPage.tscn")
		
		
	
# Called when the node enters the scene tree for the first time.
func _ready():
	add_thumbnail_buttons_to_grid()
	load_currently_set_selection()
	set_background_img()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass



