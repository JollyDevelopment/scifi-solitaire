extends TextureRect


# ----- variables -----
var progress = 0
const zero = preload("res://assets/progress_bar/bar_00.png")
const one = preload("res://assets/progress_bar/bar_01.png")
const two = preload("res://assets/progress_bar/bar_02.png")
const three = preload("res://assets/progress_bar/bar_03.png")
const four = preload("res://assets/progress_bar/bar_04.png")
const five = preload("res://assets/progress_bar/bar_05.png")
const six = preload("res://assets/progress_bar/bar_06.png")
const seven = preload("res://assets/progress_bar/bar_07.png")
const eight = preload("res://assets/progress_bar/bar_08.png")
const nine = preload("res://assets/progress_bar/bar_09.png")
const ten = preload("res://assets/progress_bar/bar_10.png")
const eleven = preload("res://assets/progress_bar/bar_11.png")
const twelve = preload("res://assets/progress_bar/bar_12.png")



# ----- my functions -----

func update_img():
	# check the progress, set the img texture accordingly
	if progress == 0:
		set_texture(zero)
	if progress == 1:
		set_texture(one)
	if progress == 2:
		set_texture(two)
	if progress == 3:
		set_texture(three)
	if progress == 4:
		set_texture(four)
	if progress == 5:
		set_texture(five)
	if progress == 6:
		set_texture(six)
	if progress == 7:
		set_texture(seven)
	if progress == 8:
		set_texture(eight)
	if progress == 9:
		set_texture(nine)
	if progress == 10:
		set_texture(ten)
	if progress == 11:
		set_texture(eleven)
	if progress == 12:
		set_texture(twelve)

func update_progress_by_one():
	progress += 1

func reset_progress():
	progress = 0


# ----- built in functions -----

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	update_img()


# ----- ready -----


# Called when the node enters the scene tree for the first time.
func _ready():
	update_img()



