extends Control

# ----- variables -----
var new_game_mouse_in = false
var how_to_play_mouse_in = false
var settings_mouse_in = false
var exit_mouse_in = false
@onready var bg_img = get_node("Background/Img")
@onready var loading_label = get_node("UI/HBC_Loading/VBC/Loading")

# --- dimensions
var i_x
var i_y
var x_5
var x_10
var x_12
var x_15
var x_20
var x_25
var x_40
var x_85
var x_90
var y_10
var y_15
var y_20
var y_25
var gui_scale
var base_logo_x = 636
var base_newgame_x = 224
var base_settings_x = 189
@onready var logo_img = get_node("UI/HBC/Elements_VBC/LogoVBC/Logo/Img")

# ----- my functions -----

func set_dimensions():
	# get window dimensions
	i_x = get_window().get_size().x
	i_y = get_window().get_size().y
	
	# set the y percents
	y_10 = i_y * .10 # 10%
	y_15 = i_y * .15 # 15%
	y_20 = i_y * .20 # 20%
	y_25 = i_y * .25 # 25%
	
	# set the x percents
	x_5 = i_x * .05 # 5%
	x_10 = i_x * .10 # 10%
	x_12 = i_x * .12 # 12%
	x_15 = i_x * .15 # 15%
	x_20 = i_x * .20 # 20%
	x_25 = i_x * .25 # 25%
	x_40 = i_x * .40 # 40%
	x_85 = i_x * .85 # 85%
	x_90 = i_x * .90 # 90%
	
func set_logo_dimensions():
	await get_tree().process_frame
	$UI/HBC/LeftBuffer.custom_minimum_size.x = x_5
	var logoscale = (x_90 / base_logo_x)
	var logoscalevec = Vector2(logoscale, logoscale)
	logo_img.scale = logoscalevec
	
func set_background_img():
	var img_path = str("res://assets/background/full/",Global.bg_num,".png")
	bg_img.set_texture(load(img_path))
	bg_img.set_position(Global.mobile_bg_position)
	Global.set_bg_img_scale(i_y)
	bg_img.set_scale(Global.mobile_bg_scale)
	bg_img.set_centered(false)
	
# ----- builtin functions -----

func _on_viewport_size_changed():
	# update the dimensions
	set_dimensions()
	set_logo_dimensions()

# --- New Game 

func _on_New_Game_Img_gui_input(event):
	if Input.is_action_just_pressed("leftclick") and new_game_mouse_in:
		# wait a beat, show the loading text
		# wait a beat again, then go load the new
		# scene. If there is no wait things are so fast
		# the loading text is not shown
		await get_tree().process_frame
		loading_label.set_visible(true)
		await get_tree().process_frame
		Global.background_load_scene("res://scenes/MobilePlayField.tscn")
	if event is InputEventScreenTouch:
		if event.is_pressed():
			# wait a beat, show the loading text
			# wait a beat again, then go load the new
			# scene. If there is no wait things are so fast
			# the loading text is not shown
			await get_tree().process_frame
			loading_label.set_visible(true)
			await get_tree().process_frame
			Global.background_load_scene("res://scenes/MobilePlayField.tscn")
		
func _on_New_Game_Img_mouse_entered():
	new_game_mouse_in = true

func _on_New_Game_Img_mouse_exited():
	new_game_mouse_in = false

# --- HowToPlay

func _on_HowToPlay_Img_mouse_entered():
	how_to_play_mouse_in = true

func _on_HowToPlay_Img_mouse_exited():
	how_to_play_mouse_in = false

func _on_HowToPlay_Img_gui_input(event):
	if Input.is_action_just_pressed("leftclick") and how_to_play_mouse_in:
		Global.goto_scene("res://scenes/HowToPlay.tscn")
	if event is InputEventScreenTouch:
		if event.is_pressed():
			Global.goto_scene("res://scenes/MobileHowToPlay.tscn")
	
# --- Settings

func _on_Settings_Img_gui_input(event):
	if Input.is_action_just_pressed("leftclick") and settings_mouse_in:
		Global.goto_scene("res://scenes/SettingsPage.tscn")
	if event is InputEventScreenTouch:
		if event.is_pressed():
			Global.goto_scene("res://scenes/MobileSettingsPage.tscn")

func _on_Settings_Img_mouse_entered():
	settings_mouse_in = true

func _on_Settings_Img_mouse_exited():
	settings_mouse_in = false
	
# --- Exit

func _on_Exit_Img_mouse_entered():
	exit_mouse_in = true

func _on_Exit_Img_mouse_exited():
	exit_mouse_in = false

func _on_Exit_Img_gui_input(event):
	if Input.is_action_just_pressed("leftclick") and exit_mouse_in:
		get_tree().quit()
	if event is InputEventScreenTouch:
		if event.is_pressed():
			get_tree().quit()


# ----- ready -----

# Called when the node enters the scene tree for the first time.
func _ready():
	# load the settings
	Global.load_settings()
	
	# set the x and y dimensions
	set_dimensions()
	
	# set the background
	set_background_img()
	
	# get the viewport size changed signal and 
	# connect the local size_changed function
	# warning-ignore:return_value_discarded
	get_tree().root.connect("size_changed", Callable(self, "_on_viewport_size_changed"))
	
	# set the logo dimensions
	set_logo_dimensions()
	
