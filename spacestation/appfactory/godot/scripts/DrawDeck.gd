extends TextureRect

# ----- variables -----
var card_array = []
var discard_deck_array = []
const one = preload("res://assets/decks/pngs/deck_one_234x332_px.png")
const two = preload("res://assets/decks/pngs/deck_two_234x332_px.png")
const three = preload("res://assets/decks/pngs/deck_three_234x332_px.png")
const empty = preload("res://assets/decks/pngs/deck_empty_234x332_px.png")
var mouse_in = false
var in_hand_instance
var play_field
var do_populate_hand = true

# ----- functions -----

# sets the image texture based on the number of cards in the 
# card_array
func set_image_texture():
	var c = card_array.size()
	if c == 0:
		set_texture(empty)
	elif c == 1:
		set_texture(one)
	elif c == 2:
		set_texture(two)
	else:
		set_texture(three)

# sets the scaling for the instance
# takes a Vector2
func set_dd_scale(vec):
	await get_tree().process_frame
	scale = vec

# add initial cards
func add_initial_cards(c_array):
#	print("initial draw deck cards amount: ", c_array.size())
	card_array = c_array
	
	
# draw_cards
# this will be called when the deck is pressed/clicked 
# it will get the first 3 cards (or two or one if thats all thats
# left) in the card_array, and pass them to the InHand instance
func draw_cards():
	play_field.discard_cards_in_hand()
	do_populate_hand = true
	
	if card_array.size() == 0:
		reset_deck_from_discard_pile()
		do_populate_hand = false
		
	if card_array.size() == 1 and do_populate_hand:
		# pop the first card off
		var c = card_array.pop_front()
		play_field.add_card_to_pending(c)
		
	if card_array.size() == 2 and do_populate_hand:
		while card_array.size() > 0:
			# pop the first card off
			var c = card_array.pop_front()
			play_field.add_card_to_pending(c)
			
	if card_array.size() == 3 and do_populate_hand:
		while card_array.size() > 0:
			# pop the first card off
			var c = card_array.pop_front()
			play_field.add_card_to_pending(c)
	
	if card_array.size() > 3 and do_populate_hand:
		var x = 2
		while x >= 0:
			# pop the first card off
			var c = card_array.pop_front()
			play_field.add_card_to_pending(c)
			x -= 1
	
	if do_populate_hand:
		# populate the hand from the pending cards
		play_field.populate_hand()
		
	do_populate_hand = true
	
func discard_card(card):
	discard_deck_array.append(card)

func reset_deck_from_discard_pile():
	# duplicate the discard array and put it in the card_array
	card_array = discard_deck_array.duplicate()
	# clear the discard array
	discard_deck_array.clear()
	
func reset_draw_deck():
	while card_array.size() > 0:
		var c = card_array.pop_front()
		c.queue_free()
	while discard_deck_array.size() > 0:
		var c = discard_deck_array.pop_front()
		c.queue_free()
	

# --- debuging functions

func report_cards():
	print("draw deck card array size: ", card_array.size())
	print("draw deck card array contents: ", card_array)
	print("draw deck discard_deck size: ", discard_deck_array.size())
	print("draw deck discard_deck: ", discard_deck_array)
	return card_array.size() + discard_deck_array.size()
	
func poll_cards():
	for c in card_array:
		print(c.name)
	
	
	
	
	
# ----- input event functions -----


func _on_Deck_mouse_entered():
	mouse_in = true

func _on_Deck_mouse_exited():
	mouse_in = false

func _on_Deck_gui_input(event):
	if Input.is_action_pressed("leftclick") and mouse_in:
		draw_cards()
	if event is InputEventScreenTouch:
		if event.is_pressed():
			draw_cards()
		

# ---- builtin functions ----

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	set_image_texture()


# ----- ready() -----

# Called when the node enters the scene tree for the first time.
func _ready():
	set_image_texture()
