extends Node2D


# ----- variables -----
var draw_deck_instance
var card_zero_pos
var card_one_pos
var card_two_pos
var card_array = []
var pending_card_array = []
const Card = preload("res://scenes/Card.tscn")

@onready var c0 = get_node("Card_00") 

# ----- my functions -----

func set_ih_scale(vec):
	scale = vec

func set_panel_size():
	self.size.x = (c0.size.x * 2)
	self.size.y = c0.size.y
	
func shift_the_cards():
	# c0 x size
	var c0x = c0.get_texture().get_width()

	# how much to shift the cards to the right
	var c1x = (c0x / 2)
	var c2x = (c1x * 2)
	
	# get the new c1 position and use that to adjust the other two cards
	# move card 2
	var c1 = get_node("Card_01")
	var c1newpos = Vector2((c0.position.x + c1x), 0)
	c1.position = c1newpos
	# move card 3
	var c2 = get_node("Card_02")
	var c2newpos = Vector2((c0.position.x + c2x), 0)
	c2.position = c2newpos
	
	# get the global positions of the cards, set them to vars
	# so the playfield can use them to add cards to
	card_zero_pos = c0.get_global_position()
	card_one_pos = c1.get_global_position()
	card_two_pos = c2.get_global_position()

func discard_cards_in_hand():
	# pass the cards back to the discard pile
	while card_array.size() > 0:
		var c = card_array.pop_front()
		print("discarding ",c)
		draw_deck_instance.discard_card(c)
	
	# remove the card instances from display
	for ci in $Card_00.get_children():
		ci.free()
	for ci in $Card_01.get_children():
		ci.free()
	for ci in $Card_02.get_children():
		ci.free()
	
func add_card_to_pending(card):
	# add the card to the in pending_card_array
	pending_card_array.append(card)
	
func populate_hand():
	while pending_card_array.size() > 0:
		var c = pending_card_array.pop_front()
		card_array.append(c)
	
	# create a new card, use the card_array card to
	# populate the new card's settings
	var c0 = Card.instantiate()
	c0.department = card_array[0].department
	c0.rank = card_array[0].rank
	c0.type = card_array[0].type
	$Card_00.add_child(c0)
	
	var c1 = Card.instantiate()
	c1.department = card_array[1].department
	c1.rank = card_array[1].rank
	c1.type = card_array[1].type
	$Card_01.add_child(c1)
	
	var c2 = Card.instantiate()
	c2.department = card_array[2].department
	c2.rank = card_array[2].rank
	c2.type = card_array[2].type
	$Card_02.add_child(c2)



# ----- built in functions -----

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	call_deferred("shift_the_cards")

# ----- ready -----


# Called when the node enters the scene tree for the first time.
func _ready():
	call_deferred("shift_the_cards")




