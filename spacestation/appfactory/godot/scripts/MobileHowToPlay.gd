extends Control


# ----- variables -----
var back_mouse_in = false
@onready var bg_img = get_node("Background/Img")

# ----- my functions ------

func set_background_img():
	var img_path = str("res://assets/background/full/",Global.bg_num,".png")
	bg_img.set_texture(load(img_path))
	bg_img.position = Global.mobile_bg_position
	Global.set_bg_img_scale(get_window().get_size().y)
#	bg_img.scale = Global.bg_scale
	bg_img.scale = Global.mobile_bg_scale
	bg_img.centered = false


# ----- built in functions -----

func _on_Back_Text_mouse_entered():
	back_mouse_in = true

func _on_Back_Text_mouse_exited():
	back_mouse_in = false

func _on_Back_Text_gui_input(event):
	if Input.is_action_just_pressed("leftclick") and back_mouse_in:
		Global.goto_scene("res://scenes/LandingPage.tscn")
	if event is InputEventScreenTouch:
		if event.is_pressed():
			Global.goto_scene("res://scenes/MobileLandingPage.tscn")

# ----- ready -----

# Called when the node enters the scene tree for the first time.
func _ready():
	set_background_img()




