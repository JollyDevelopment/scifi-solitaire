extends HBoxContainer


# ----- variable -----
var backgroundloader
var wait_frames
var time_max = 10 # msec
var root
var current_scene = null
var story_mode
#onready var pb = get_node("VBC/ProgressBar")
var pb
#var spinner
var current_os
var a_story_mode_popup_is_open = false
var config

# --- background img settings
var bg_num
#var bg_position = Vector2(900,650)
var bg_position = Vector2(0,0)
var mobile_bg_position = Vector2(0,0)
var bg_scale = Vector2(1.165,1.165)
var mobile_bg_scale

# --- card color set choice
var card_color_set

# ----- constants
#var settings_file = "user://settings.save"
var config_file = "user://settings.cfg"
var settings_section = "settings"


# ----- instances


# --- dimensions


# ----- my functions -----

	
func goto_scene(path):
#	# This function will usually be called from a signal callback,
#	# or some other function in the current scene.
#	# Deleting the current scene at this point is
#	# a bad idea, because it may still be executing code.
#	# This will result in a crash or unexpected behavior.
#
#	# The solution is to defer the load to a later time, when
#	# we can be sure that no code from the current scene is running:
	call_deferred("_deferred_goto_scene", path)

func _deferred_goto_scene(path):
#	# It is now safe to remove the current scene
	current_scene.free()
#	# Load the new scene.
	var s = ResourceLoader.load(path)
#	# Instance the new scene.
	current_scene = s.instantiate()
#	# Add it to the active scene, as child of root.
	get_tree().get_root().add_child(current_scene)
#	# Optionally, to make it compatible with the SceneTree.change_scene() API.
	get_tree().set_current_scene(current_scene)

func save_settings():
	# attempt to load the config
	config = ConfigFile.new()
	var err = config.load(config_file)
	# if err is not OK then lets just run the load and initiate defaults
	if err != OK:
		load_settings()
	else:
		config.set_value(settings_section, "bg_num", bg_num)
		config.set_value(settings_section, "card_color_set", card_color_set)
		config.set_value(settings_section, "story_mode", story_mode)
		# save to disk
		config.save(config_file)

func load_settings():
	# attempt to load the config
	config = ConfigFile.new()
	var err = config.load(config_file)
	
	# if the err is not OK
	# lets assume its a first run and make the file
	# and populate it with default settings
	if err != OK:
		# this sets defaults for the settings.cfg, 
		# for a first run
		config.set_value(settings_section, "bg_num", "0")
		config.set_value(settings_section, "card_color_set", "0")
		config.set_value(settings_section, "story_mode", false)
		# save to disk
		config.save(config_file)
		# this sets the in-memory settings for a first run
		bg_num = "0"
		card_color_set = "0"
		story_mode = false
	else:
		bg_num = config.get_value(settings_section, "bg_num")
		card_color_set = config.get_value(settings_section, "card_color_set")
		story_mode = config.get_value(settings_section, "story_mode")
		
	#print("load settings bg_num: ", bg_num)
	#print("load settings card_color: ", card_color_set)
	#print("load settings story_mode: ", story_mode)
		
func background_load_scene(path):
	# create a loader, give it the scene path to load
	ResourceLoader.load_threaded_request(path)
	
	# while loop to check if the load is done
	while ResourceLoader.load_threaded_get_status(path) != 3:
		continue
		
	# scene is loaded, get the resource and swap to it
	var resource = ResourceLoader.load_threaded_get(path)
	swap_to_backgound_loaded_scene(resource)
	
func update_progress_bar():
	# get the current loading step, divide it 
	# by the total amount of loading to do,
	# that will be the value passed to the progress bar
	var progress = float(backgroundloader.get_stage() / backgroundloader.get_stage_count())
	
	# set that as the value for the progress bar
	pb.set_value(progress)

func swap_to_backgound_loaded_scene(scene_resource):
	# set the current_scene to disappear
	current_scene.queue_free()
	# set the new scene to be the new current scene and 
	# attach it to the tree
	current_scene = scene_resource.instantiate()
	get_node("/root").add_child.call_deferred(current_scene)
	
func get_current_os():
	current_os = OS.get_name()
	
func set_bg_img_scale(screen_y):
	var img_path = str("res://assets/background/full/",bg_num,".png")
	var tmp_img = Sprite2D.new()
	tmp_img.set_texture(load(img_path))
	if current_os == "Android":
		# convert the values to float() otherwise
		# they will be rounded and I need the 0.1234 version
		# for the scaling
		var bgs = float(screen_y) / float(tmp_img.get_texture().get_height())
		mobile_bg_scale = Vector2(bgs, bgs)
	else:
		# figure out which float is bigger, then do the divide based
		# on that
		if float(tmp_img.get_texture().get_height()) > float(screen_y):
			var bgs = float(tmp_img.get_texture().get_height()) / float(screen_y) 
			bg_scale = Vector2(bgs, bgs)
		else:
			var bgs = float(screen_y) / float(tmp_img.get_texture().get_height())  
			bg_scale = Vector2(bgs, bgs)
	
# ----- built in functions -----

# ----- ready -----

func _ready():
	root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() - 1)
	get_current_os()

