extends Node2D


# ---- variables -----
var roster
var ship_class = "frigate"
var ship_captain
var departments
var scenario_num
var raw_scenarios_list
var filled_scenarios_list

# ---- contants ----
var sp_file_path = "res://assets/encounters/encounters.json"
var pp_file_path = "res://assets/drydock/personnel/pronouns/list.json"
var dp_file_path = "res://assets/drydock/fleet/assignments/departments.json"


# ----- functions -----

func get_departments(r):
	var deps = []
	for d in r.keys():
		deps.append(d)
	return deps

func load_scenario_pool():
	var sp = {}
	var sp_file = FileAccess.open(sp_file_path, FileAccess.READ)
	#sp_file.open("res://assets/encounters/encounters.json", sp_file.READ)
	var sp_text = sp_file.get_as_text()
	# setup the json interface
	var json = JSON.new()
	# try to get the data
	var error = json.parse(sp_text)
	# check if it errored, if not load it and 
	# check if its the right stuff
	if error == OK:
		sp = json.data
	#var test_json_conv = JSON.new()
	#test_json_conv.parse(sp_text)
	#var sp_json_parse = test_json_conv.get_data()
	#var sp = sp_json_parse.result
	sp_file.close()
	return sp
	
func load_pronouns_pool():
	var pp = {}
	var pp_file = FileAccess.open(pp_file_path, FileAccess.READ)
	#pp_file.open("res://assets/drydock/personnel/pronouns/list.json", pp_file.READ)
	var pp_text = pp_file.get_as_text()
	# setup the json interface
	var json = JSON.new()
	# try to get the data
	var error = json.parse(pp_text)
	# check if it errored, if not load it and 
	# check if its the right stuff
	if error == OK:
		pp = json.data
	#var test_json_conv = JSON.new()
	#test_json_conv.parse(pp_text)
	#var pp_json_parse = test_json_conv.get_data()
	#var pp = pp_json_parse.result
	pp_file.close()
	return pp
	
func get_scenario_num():
	var scen_pool = load_scenario_pool()
	var scen_num = scen_pool[ship_class]['scenarios_per']
	return scen_num

func load_departments_pool():
	var dp = {}
	var dp_file = FileAccess.open(dp_file_path, FileAccess.READ)
	#dp_file.open("res://assets/drydock/fleet/assignments/departments.json", dp_file.READ)
	var dp_text = dp_file.get_as_text()
	# setup the json interface
	var json = JSON.new()
	# try to get the data
	var error = json.parse(dp_text)
	# check if it errored, if not load it and 
	# check if its the right stuff
	if error == OK:
		dp = json.data
	#var test_json_conv = JSON.new()
	#test_json_conv.parse(dp_text)
	#var dp_json_parse = test_json_conv.get_data()
	#var dp = dp_json_parse.result
	dp_file.close()
	return dp
	
func build_scenarios_list(sclass, deps):
	# load the scenarios pool
	var spool = load_scenario_pool() 
	
	# for the ship_class['scenarios_per] / num_of_departments get that many random scenarios
	# so for the 'frigate' class, that will be: scenarios_per(12) divided by dep_num(3) = 4 random scenarios for
	# each department
	var bsl_sp = spool[sclass]['scenarios_per']
	var bsl_dn = departments.size()
	var bsl_scen_num = bsl_sp / bsl_dn 
	
	# get the ship_class scenarios from scenario pool
	var ship = spool[sclass]
	
	# create the dict to hold the full scenario list
	var full_scenario_dict = {}
	
	# for each department, pick a random scenario 'scen_num' of times. 
	# Make sure the scenarios do not duplicate
	for d in deps:
		# load the scenarios for that dep
		var sd = ship[d]
		
		# get a list of the keys in the dict
		# while the scenario_list has less items that the scenario_num
		# choose a random key
		# get the index number of that choice
		# pop off that item and add it to the num_list
		var scenario_keys = sd.keys()
		var dep_scenario_list = []
		while dep_scenario_list.size() < bsl_scen_num:
			# choose a random key
			var rng = RandomNumberGenerator.new()
			rng.randomize()
			var randchoice = rng.randi_range(0, (scenario_keys.size() - 1))
			var chosenscenario_num = scenario_keys[randchoice]
			# if the chosenscenario_num already is in the array
			# don't add it again, but restart the loop to get a different num
			if chosenscenario_num in dep_scenario_list:
				continue
			dep_scenario_list.append(chosenscenario_num)
		
		# dict to hold the department that is being iterated over's selected scenarios
		var department_scenarios = {}
		# add the currently selected scenarios to the department_scenarios
		for scenario_number in dep_scenario_list:
			department_scenarios[scenario_number] = sd[scenario_number]
		
		# add the department_scenarios to the full_scenario_dict
		full_scenario_dict[d] = department_scenarios
		
		# reset scenario_keys and dep_scenario_list
		scenario_keys = []
		dep_scenario_list = []
		
	return full_scenario_dict
	
func build_filled_scenarios(raw_scenarios, ship_roster, ship_captain, ship_class):
	# run the replacements
	var filled_scenarios = run_replacements(ship_captain, roster, raw_scenarios, ship_class)
	return filled_scenarios
	
func run_replacements(captain, roster, raw_scenarios, ship_class):
	# copy the raw dict to fill and pass back
	var rr_filled_scenarios = raw_scenarios.duplicate()
	
	# load the departments file
	var df = load_departments_pool()
	
	# get the officers per department
	var op = df[ship_class]['officers_per']
	
	# nested for loops
	# for each key in the raw_scenario.keys() (which are the departments, so for each department)
	# load that key's value ( which will be all the scenarios for that department
	# for each key in that dict (which will be the scenario's number)
	# take the content of the Officer_Order, Crew_Initiate_Action_Description, and Crew_Complete_Action_Description
	# convert that content to a string
	# run replace on it for the officers/captain, and the crew members
	for department in raw_scenarios.keys():
		for scenario in raw_scenarios[department].keys():
			# get the officer string
			var officer_order_str = raw_scenarios[department][scenario]['Officer_Order']
			# replace captain name/last name
			officer_order_str = officer_order_str.replace('$CPT_N', ship_captain.full_name)
			officer_order_str = officer_order_str.replace('$CPT_LN', ship_captain.last_name)
			
			# officer replacements
			var x = 1
			while x < (op+1):
				# get the matching departments officer x from the roster
#				var o = load(roster[department]['staff'][x])
				var ofr = '$O%s_R' % str(x) # build the officers rank string
				var ofn = '$O%s_N' % str(x) # build the officers full name string
				var ofln = '$O%s_LN' % str(x) # build the officers last name string
				var oppesu = '$O%s_P_Pe_Su' % str(x) # build the officers personal subjective pronoun
				var oppesuc = '$O%s_P_Pe_Su_C' % str(x) # build the officers capitalized personal subjective pronoun
				var oppeob = '$O%s_P_Pe_Ob' % str(x) # build the officers personal objective pronoun
				var oppeobc = '$O%s_P_Pe_Ob_C' % str(x) # build the officers capitalized personal objective pronoun
				var oppo = '$O%s_P_Po' % str(x) # build the officers possessive pronoun
				var oppoc = '$O%s_P_Po_C' % str(x) # build the officers capitalized possessive pronoun
				var opre = '$O%s_P_Re' % str(x) # build the officers reflexive pronoun
				
				# run through the pronoun replacements
				# rank
				officer_order_str = officer_order_str.replace(ofr, roster[department]['staff'][x].rank)
				# full name
				officer_order_str = officer_order_str.replace(ofn, roster[department]['staff'][x].full_name)
				# last name
				officer_order_str = officer_order_str.replace(ofln, roster[department]['staff'][x].last_name)
				# pronouns
				officer_order_str = officer_order_str.replace(oppesuc, roster[department]['staff'][x].P_Pe_Su_C)
				officer_order_str = officer_order_str.replace(oppesu, roster[department]['staff'][x].P_Pe_Su)
				officer_order_str = officer_order_str.replace(oppeobc, roster[department]['staff'][x].P_Pe_Ob_C)
				officer_order_str = officer_order_str.replace(oppeob, roster[department]['staff'][x].P_Pe_Ob)
				officer_order_str = officer_order_str.replace(oppoc, roster[department]['staff'][x].P_Po_C)
				officer_order_str = officer_order_str.replace(oppo, roster[department]['staff'][x].P_Po)
				officer_order_str = officer_order_str.replace(opre, roster[department]['staff'][x].P_Re)
				
				# iterate x
				x = x+1
			
			# update the filled scenarios dict with the updated string
			rr_filled_scenarios[department][scenario]['Officer_Order'] = officer_order_str
			
			# get the crew action strings
			var crew_ia_desc_str = raw_scenarios[department][scenario]['Crew_Initiate_Action_Description']
			var crew_ca_desc_str = raw_scenarios[department][scenario]['Crew_Complete_Action_Description']
			
			# crew replacements
			var y = 1
			while y <= (op+1):
				# get the matching departments crew member y's from the roster
				var cr = '$C0%s_R' % str(y)  # build the crew members rank string
				var cn = '$C0%s_N' % str(y)  # build the crew members full name string
				var cln = '$C0%s_LN' % str(y)  # build the crew members last name string
				var cppesu = '$C0%s_P_Pe_Su' % str(y)  # build the crew members personal subjective pronoun
				var cppesuc = '$C0%s_P_Pe_Su_C' % str(y)  # build the crew members capitalized personal subjective pronoun
				var cppeob = '$C0%s_P_Pe_Ob' % str(y)  # build the crew members personal objective pronoun
				var cppeobc = '$C0%s_P_Pe_Ob_C' % str(y)  # build the crew members capitalized personal objective pronoun
				var cppo = '$C0%s_P_Po' % str(y)  # build the crew members possessive pronoun
				var cppoc = '$C0%s_P_Po_C' % str(y)  # build the crew members capitalized possessive pronoun
				var cpre = '$C0%s_P_Re' % str(y)  # build the crew members reflexive pronoun
				
				# run through the pronoun replacements
				# rank
				crew_ia_desc_str = crew_ia_desc_str.replace(cr, roster[department]['crew'][y].rank)
				crew_ca_desc_str = crew_ca_desc_str.replace(cr, roster[department]['crew'][y].rank)
				# full name
				crew_ia_desc_str = crew_ia_desc_str.replace(cn, roster[department]['crew'][y].first_name + " " + roster[department]['crew'][y].last_name)
				crew_ca_desc_str = crew_ca_desc_str.replace(cn, roster[department]['crew'][y].first_name + " " + roster[department]['crew'][y].last_name)
				# last name
				crew_ia_desc_str = crew_ia_desc_str.replace(cln, roster[department]['crew'][y].last_name)
				crew_ca_desc_str = crew_ca_desc_str.replace(cln, roster[department]['crew'][y].last_name)
				# pronouns
				crew_ia_desc_str = crew_ia_desc_str.replace(cppesuc, roster[department]['crew'][y].P_Pe_Su_C)
				crew_ia_desc_str = crew_ia_desc_str.replace(cppesu, roster[department]['crew'][y].P_Pe_Su)
				crew_ia_desc_str = crew_ia_desc_str.replace(cppeobc, roster[department]['crew'][y].P_Pe_Ob_C)
				crew_ia_desc_str = crew_ia_desc_str.replace(cppeob, roster[department]['crew'][y].P_Pe_Ob)
				crew_ia_desc_str = crew_ia_desc_str.replace(cppoc, roster[department]['crew'][y].P_Po_C)
				crew_ia_desc_str = crew_ia_desc_str.replace(cppo, roster[department]['crew'][y].P_Po)
				crew_ia_desc_str = crew_ia_desc_str.replace(cpre, roster[department]['crew'][y].P_Re)

				crew_ca_desc_str = crew_ca_desc_str.replace(cppesuc, roster[department]['crew'][y].P_Pe_Su_C)
				crew_ca_desc_str = crew_ca_desc_str.replace(cppesu, roster[department]['crew'][y].P_Pe_Su)
				crew_ca_desc_str = crew_ca_desc_str.replace(cppeobc, roster[department]['crew'][y].P_Pe_Ob_C)
				crew_ca_desc_str = crew_ca_desc_str.replace(cppeob, roster[department]['crew'][y].P_Pe_Ob)
				crew_ca_desc_str = crew_ca_desc_str.replace(cppoc, roster[department]['crew'][y].P_Po_C)
				crew_ca_desc_str = crew_ca_desc_str.replace(cppo, roster[department]['crew'][y].P_Po)
				crew_ca_desc_str = crew_ca_desc_str.replace(cpre, roster[department]['crew'][y].P_Re)
				
				# iterate y
				y = y + 1
				
			# updated the filled scenarions dict with the updated strings
			rr_filled_scenarios[department][scenario]['Crew_Initiate_Action_Description'] = crew_ia_desc_str
			rr_filled_scenarios[department][scenario]['Crew_Complete_Action_Description'] = crew_ca_desc_str
			
	return rr_filled_scenarios
			
			
# Called when the node enters the scene tree for the first time.
func _ready():
	# get the departments
	departments = get_departments(roster)
	# get the number of scenarios per department to populate
	scenario_num = get_scenario_num()
	# get the raw scenarios
	raw_scenarios_list = build_scenarios_list(ship_class, departments)
	# fill in the search and replace pronouns and names
	filled_scenarios_list = build_filled_scenarios(raw_scenarios_list, roster, ship_captain, ship_class)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
