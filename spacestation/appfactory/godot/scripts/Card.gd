extends Area2D


# ----- variables -----
var ccs = Global.card_color_set
#var ccs = "default"
#var card_img_path
# --- onready
@onready var card_img_path = str("res://assets/cards/",department,"/",rank,"/",ccs,"/",type,".png")

# --- local vars
var department = "support"
var rank = "crew"
var type = "complete_action"
var mouse_in = false
var dragging = false
var mouse_pos
var cs_signal_sent = false
var collision_shape_orig_extents = Vector2(117.5,166)
var collision_shape_orig_pos = Vector2(0,0)
var card_match
var is_c0 = false
var is_c1 = false
var is_c2 = false

# --- instance references
var playfield_instance

# --- signals
signal currently_selected



# ----- my functions -----

func set_img_texture():
#	Global.load_settings()
#	var ccs = Global.card_color_set
#	card_img_path = str("res://assets/cards/",department,"/",rank,"/",ccs,"/",type,".png")
	$Img.set_texture(load(card_img_path))

func add_to_groups():
	add_to_group(department)
	add_to_group(rank)
	add_to_group(type)

func enable_collision_shape():
	$CollShape.disabled = false

func connect_currently_selected_signal():
	# create strings to match the playfield encounterstack functions
	var my_parent_name = get_parent().name
	var playfield_currently_selected_card_function = str("_",my_parent_name,"_card_selected")
	# connect this instances card match signals to the playfield functions
	if playfield_instance:
		if playfield_instance.has_method(playfield_currently_selected_card_function):
#			print(self.name, " connecting ", playfield_currently_selected_card_function)
			connect("currently_selected", Callable(playfield_instance, playfield_currently_selected_card_function))

func disconnect_currently_selected_signal():
	# create strings to match the playfield encounterstack functions
	var my_parent_name = get_parent().name
	var playfield_currently_selected_card_function = str("_",my_parent_name,"_card_selected")
	# connect this instances card match signals to the playfield functions
	if playfield_instance:
		if playfield_instance.has_method(playfield_currently_selected_card_function):
#			print(self.name, " disconnecting ", playfield_currently_selected_card_function)
			disconnect("currently_selected", Callable(playfield_instance, playfield_currently_selected_card_function))

func set_department(dep):
	self.department = dep
	
# ----- builtin functions -----

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if dragging:
		mouse_pos = get_viewport().get_mouse_position()
		self.position = mouse_pos

func _on_Card_input_event(viewport, event, shape_idx):
	if Input.is_action_pressed("leftclick"):
		dragging = true
		if not cs_signal_sent:
			emit_signal("currently_selected")
			cs_signal_sent = true
		self.get_viewport().set_input_as_handled()
		$Img.centered = true
		#$CollShape.get_shape().set_extents(Vector2(60,60))
		$CollShape.get_shape().set_size(Vector2(60,60))
		$CollShape.position = Vector2(0,50)
	if event is InputEventScreenTouch:
		if event.is_pressed():
			dragging = true
		if not cs_signal_sent:
			emit_signal("currently_selected")
			cs_signal_sent = true
		self.get_viewport().set_input_as_handled()
		$Img.centered = true
		$CollShape.get_shape().set_size(Vector2(60,60))
		$CollShape.position = Vector2(0,50)
		

func _unhandled_input(event):
	# have to catch the released button in an unhandled event, not sure why 
	# yet
	if Input.is_action_just_released("leftclick"):
		dragging = false
		cs_signal_sent = false
		if mouse_in:
			#$CollShape.get_shape().set_extents(collision_shape_orig_extents)
			$CollShape.get_shape().set_size(collision_shape_orig_extents)
			$CollShape.position = collision_shape_orig_pos

func _on_Card_mouse_entered():
	mouse_in = true

func _on_Card_mouse_exited():
	mouse_in = false

func _exit_tree():
	disconnect_currently_selected_signal()
		
		
# ----- ready -----

# Called when the node enters the scene tree for the first time.
func _ready():
	set_img_texture()
	add_to_groups()
	call_deferred("connect_currently_selected_signal")






