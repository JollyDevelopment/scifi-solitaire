extends Button


# ----- variables -----
var base_path = "res://assets/cards/thumbs/"
var img_num
var img_scale_percent = 1
var base_thumb_x = 512 # 256
var base_thumb_y = 300 # 150

# --- signals
signal selection(number)

# --- instance refs
var card_theme_selector_page

# ----- my functions -----
func set_img(num):
	img_num = str(num)
	var path = str(base_path,img_num,".png")
	$Img.set_texture(load(path))

func set_img_scale_percent(percent):
	img_scale_percent = percent
	
func set_img_scale():
	# get the buttons current x
	# get the percent of that (percent * current x)
	# divide that by the base img size to get the new scale
	var c_x = get_size().x
	var c_y = get_size().y
	var sx = (img_scale_percent * c_x)
	var sy = (img_scale_percent * c_y)
	var xscale = (sx / base_thumb_x)
	var yscale = (sy / base_thumb_y)
	$Img.set_scale(Vector2(xscale,yscale))
	
func set_img_position():
	var b_x = get_size().x
	var b_y = get_size().y
	var halfx = (b_x / 2)
	var halfy = (b_y /2)
	$Img.set_position(Vector2(halfx,halfy))

func connect_selection_signal():
	connect("selection", Callable(card_theme_selector_page, "_set_card_theme_selection"))

func unselect_card_theme():
	if is_pressed():
		set_pressed(false)
		
func set_as_selected():
	set_pressed(true)
	

# ----- built in functions -----

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	set_img_position()
	set_img_scale()


func _on_CardThemeThumbButton_pressed():
	emit_signal("selection", self.name)
	
	
# ----- ready -----

# Called when the node enters the scene tree for the first time.
func _ready():
#	set_img(0)
	connect_selection_signal()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass



