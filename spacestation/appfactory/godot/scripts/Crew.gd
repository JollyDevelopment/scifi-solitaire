extends Node2D


# note to self: this is meant to be the equivalent to
# the cardfactory's crew.py

# ----- variables -----
var non_com
var rank
var sex
var pronouns
var first_name
var last_name
var full_name
var department
var P_Pe_Su
var P_Pe_Su_C
var P_Pe_Ob
var P_Pe_Ob_C
var P_Po
var P_Po_C
var P_Re

# ---- contants ----
var cranks_file_path = "res://assets/drydock/personnel/ranks/crew_ranks.json"
var pro_file_path = "res://assets/drydock/personnel/pronouns/list.json"
var fnames_file_path = "res://assets/drydock/personnel/onomastics/first_names_female.json"
var mnames_file_path = "res://assets/drydock/personnel/onomastics/first_names_male.json"
var nnames_file_path = "res://assets/drydock/personnel/onomastics/first_names_neuter.json"
var lnames_file_path = "res://assets/drydock/personnel/onomastics/last_names.json"



# ----- functions -----

func assign_rank(nc):
	# get the ranks file and parse it
	var cranks = {}
	var cranks_file = FileAccess.open(cranks_file_path, FileAccess.READ)
	#cranks_file.open("res://assets/drydock/personnel/ranks/crew_ranks.json", cranks_file.READ)
	var cranks_text = cranks_file.get_as_text()
	# setup the json interface
	var json = JSON.new()
	# try to get the data
	var error = json.parse(cranks_text)
	# check if it errored, if not load it and 
	# check if its the right stuff
	if error == OK:
		cranks = json.data
	#var test_json_conv = JSON.new()
	#test_json_conv.parse(cranks_text)
	#var cranks_json_parse = test_json_conv.get_data()
	#cranks = cranks_json_parse.result
	cranks_file.close()
	
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	
	if nc:
		return cranks[str(rng.randi_range(6,9))]
	else:
		return cranks[str(rng.randi_range(1,5))]

func decide_sex():
	var my_sex = randi() % 3 + 1
	if my_sex == 1:
		return "F"
	elif my_sex == 2:
		return "M"
	elif my_sex == 3:
		return "N"

func get_pronouns(sex):
	var pro_file = FileAccess.open(pro_file_path, FileAccess.READ)
	#pro_file.open("res://assets/drydock/personnel/pronouns/list.json", pro_file.READ)
	var pro_text = pro_file.get_as_text()
	# setup the json interface
	var json = JSON.new()
	# try to get the data
	var error = json.parse(pro_text)
	# check if it errored, if not load it and 
	# check if its the right stuff
	if error == OK:
		pronouns = json.data
	#var test_json_conv = JSON.new()
	#test_json_conv.parse(pro_text)
	#var pro_json_parse = test_json_conv.get_data()
	#pronouns = pro_json_parse.result
	pro_file.close()
	return pronouns[sex]

func get_first_name(s):
	var names = {}
	if s == "F":
		var fnames_file = FileAccess.open(fnames_file_path, FileAccess.READ)
		#fnames_file.open("res://assets/drydock/personnel/onomastics/first_names_female.json", fnames_file.READ)
		var fnames_text = fnames_file.get_as_text()
		# setup the json interface
		var json = JSON.new()
		# try to get the data
		var error = json.parse(fnames_text)
		# check if it errored, if not load it and 
		# check if its the right stuff
		if error == OK:
			names = json.data
		#var test_json_conv = JSON.new()
		#test_json_conv.parse(fnames_text)
		#var fnames_json_parse = test_json_conv.get_data()
		#names = fnames_json_parse.result
		fnames_file.close()
	elif s == "M":
		var mnames_file = FileAccess.open(fnames_file_path, FileAccess.READ)
		#mnames_file.open("res://assets/drydock/personnel/onomastics/first_names_male.json", mnames_file.READ)
		var mnames_text = mnames_file.get_as_text()
		# setup the json interface
		var json = JSON.new()
		# try to get the data
		var error = json.parse(mnames_text)
		# check if it errored, if not load it and 
		# check if its the right stuff
		if error == OK:
			names = json.data
		#var test_json_conv = JSON.new()
		#test_json_conv.parse(mnames_text)
		#var mnames_json_parse = test_json_conv.get_data()
		#names = mnames_json_parse.result
		mnames_file.close()
	else:
		var nnames_file = FileAccess.open(nnames_file_path, FileAccess.READ)
		#nnames_file.open("res://assets/drydock/personnel/onomastics/first_names_neuter.json", nnames_file.READ)
		var nnames_text = nnames_file.get_as_text()
		# setup the json interface
		var json = JSON.new()
		# try to get the data
		var error = json.parse(nnames_text)
		# check if it errored, if not load it and 
		# check if its the right stuff
		if error == OK:
			names = json.data
		#var test_json_conv = JSON.new()
		#test_json_conv.parse(nnames_text)
		#var nnames_json_parse = test_json_conv.get_data()
		#names = nnames_json_parse.result
		nnames_file.close()
	
	# set the first name an return it
	var f_name = names[randi() % names.size()]
	return f_name 
		
func get_last_name():
	var lnames = {}
	var lnames_file = FileAccess.open(lnames_file_path, FileAccess.READ)
	#lnames_file.open("res://assets/drydock/personnel/onomastics/last_names.json", lnames_file.READ)
	var lnames_text = lnames_file.get_as_text()
	# setup the json interface
	var json = JSON.new()
	# try to get the data
	var error = json.parse(lnames_text)
	# check if it errored, if not load it and 
	# check if its the right stuff
	if error == OK:
		lnames = json.data
	#var test_json_conv = JSON.new()
	#test_json_conv.parse(lnames_text)
	#var lnames_json_parse = test_json_conv.get_data()
	#var lnames = lnames_json_parse.result
	var l_name = lnames[randi() % lnames.size()]
	return l_name.to_lower().capitalize()



# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	rank = assign_rank(non_com)
	sex = decide_sex()
	pronouns = get_pronouns(sex)
	first_name = get_first_name(sex)
	last_name = get_last_name()
	full_name = first_name + " " + last_name
	
	# set this crew members pronouns
	P_Pe_Su = pronouns['personal']['subjective']
	P_Pe_Su_C = pronouns['personal']['subjective_capitalized']
	P_Pe_Ob = pronouns['personal']['objective']
	P_Pe_Ob_C = pronouns['personal']['objective_capitalized']
	P_Po = pronouns['possessive']
	P_Po_C = pronouns['possessive_capitalized']
	P_Re = pronouns['reflexive']
	

