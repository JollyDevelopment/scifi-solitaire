extends MarginContainer


# ----- variables -----
var base_path = "res://assets/background/thumb/"


# ----- my functions ------

func set_img(num):
	var path = str(base_path,num,".png")
	$CC/Img.set_texture(load(path))

# ----- built in functions -----


# ----- ready -----

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
