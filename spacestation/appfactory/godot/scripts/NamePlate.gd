extends Node2D

# ----- variables -----
@onready var label = get_node("CenterContainer/Label")
@onready var bb = get_node("BoundingBox")
var font

# ------ my functions -----

func decrease_font_size_by_one():
	var current_size = label.get_theme_font_size("font_size")
	var new_size = (current_size - 1)
	label.add_theme_font_size_override("font_size", new_size)
	
func increase_font_size_by_one():
	var current_size = label.get_theme_font_size("font_size")
	var new_size = (current_size + 1)
	label.add_theme_font_size_override("font_size", new_size)

func adjust_cpt_name_to_fit():
	# get font
	font = label.get_theme_font("font")
	# get font size
	var font_size = label.get_theme_font_size("font_size")
	# get bounding box width
	var box_width = bb.size.x
	# get current label text
	var current_text = label.text
	# set ideal width as slightly smaller than 
	# the bounding box
	var ideal_text_width = bb.size.x - 8
	# loop ten times
	# for font size adjustments
	for x in range(9):
		# get font size
		var current_font_size = label.get_theme_font_size("font_size")
		# get the width of the current label text 
		# rendered as the current theme font + font size
		var current_text_width = font.get_string_size(current_text, HORIZONTAL_ALIGNMENT_LEFT, -1, current_font_size).x
		# if too big, reduce font size by one
		if current_text_width > ideal_text_width:
			decrease_font_size_by_one()
		elif current_text_width < ideal_text_width:
			increase_font_size_by_one()
	
# ----- built in functions -----



# ----- ready -----

# Called when the node enters the scene tree for the first time.
func _ready():
	adjust_cpt_name_to_fit()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
