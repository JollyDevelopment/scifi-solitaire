extends RigidBody2D

# ----- variables -----
@export var min_speed = 250
@export var max_speed = 450


# ----- my functions -----

func set_ship_scale(vec):
	$Img.scale = vec
	
func set_ship_angle(angle):
	$Img.set_rotation_degrees(angle)

