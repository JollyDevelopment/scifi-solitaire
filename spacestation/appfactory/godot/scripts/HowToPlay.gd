extends Control


# ----- variables -----
var i_x
var i_y
var back_mouse_in = false
@onready var bg_img = get_node("Background/Img")

# ----- my functions ------

func set_dimensions():
	# get window dimensions
	i_x = get_window().get_size().x
	i_y = get_window().get_size().y
	
func set_background_img():
	var img_path = str("res://assets/background/full/",Global.bg_num,".png")
	bg_img.set_texture(load(img_path))
	bg_img.set_position(Global.bg_position)
	Global.set_bg_img_scale(i_y)
	bg_img.set_scale(Global.bg_scale)
	bg_img.set_centered(false)


# ----- built in functions -----

func _on_Back_Text_mouse_entered():
	back_mouse_in = true

func _on_Back_Text_mouse_exited():
	back_mouse_in = false

func _on_Back_Text_gui_input(event):
	if Input.is_action_just_pressed("leftclick") and back_mouse_in:
		Global.goto_scene("res://scenes/LandingPage.tscn")
	if event is InputEventScreenTouch:
		if event.is_pressed():
			Global.goto_scene("res://scenes/LandingPage.tscn")

# ----- ready -----

# Called when the node enters the scene tree for the first time.
func _ready():
	set_dimensions()
	set_background_img()




