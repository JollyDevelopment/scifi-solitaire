extends Window

# ----- variables -----
var encounter = {}
var test_encounter = {"Title": "Distress Call - Pirates!", "Description": "In the depths of the cold void, any cry for help must be answered! The honor of a ships crew demands that they come to the aid of any in need. Beware however for not all ships feel the same way...", "Officer_Order": "$O1_R $O1_LN reports a new signal. Its a distress call! We have to answer, but be cautious, it could be a trick!", "Crew_Initiate_Action_Description": "$C01_R $C01_N examines the data and ship profiles. $C01_P_Pe_Su_C tweaks the long range sensors for maximum gain.", "Crew_Complete_Action_Description": "$C02_R $C02_N spots a second stealth ship hiding behind the “distressed merchant”, in just enough time for the pilot to reroute around the pirates maximum range and get the ship to safety." }
@onready var oo = get_node("MC/HBC/VBC/OfficerOrder")
@onready var cia = get_node("MC/HBC/VBC/CIA")
@onready var cca = get_node("MC/HBC/VBC/CCA")
#@onready var bg_img = get_node("BG_IMG")

# --- instance references
var playfield_instance

# ----- signals -----
signal popup_closed

# ----- my functions -----

func add_encounter_dict(e):
	self.set_title(e["Title"])
	oo.text = e["Officer_Order"]
	cia.text = e["Crew_Initiate_Action_Description"]
	cca.text = e["Crew_Complete_Action_Description"]
	
func show_encounter_popup():
	Global.a_story_mode_popup_is_open = true
	#self.popup_window = true
	popup_centered_ratio(0.5)
	
func _react_to_popup_closing():
	Global.a_story_mode_popup_is_open = false
	self.hide()
	emit_signal("popup_closed")
	
func connect_popup_closed_signal():
	if playfield_instance:
		connect("popup_closed", Callable(playfield_instance, "check_for_win_condition"))
			
func disconnect_popup_closed_signal():
	if playfield_instance:
		disconnect("popup_closed", Callable(playfield_instance, "check_for_win_condition"))
		
# ------ built in functions ------

func _exit_tree():
	disconnect_popup_closed_signal()

func _on_close_requested():
	_react_to_popup_closing()

# ----- ready ------


# Called when the node enters the scene tree for the first time.
func _ready():
	connect_popup_closed_signal()

