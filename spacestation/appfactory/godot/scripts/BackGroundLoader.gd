extends HBoxContainer


# ----- variable -----
var backgroundloader
var wait_frames
var time_max = 100 # msec
var root
var current_scene = null
var story_mode = false
@onready var pb = get_node("VBC/ProgressBar")

# --- background img settings
var bg_num
var bg_position = Vector2(900,650)
var bg_scale = Vector2(1.165,1.165)

# ----- constants
var settings_file = "user://settings.save"


# ----- instances


# --- dimensions


# ----- my functions -----

	
func goto_scene(path):
#	# This function will usually be called from a signal callback,
#	# or some other function in the current scene.
#	# Deleting the current scene at this point is
#	# a bad idea, because it may still be executing code.
#	# This will result in a crash or unexpected behavior.
#
#	# The solution is to defer the load to a later time, when
#	# we can be sure that no code from the current scene is running:
#
	call_deferred("_deferred_goto_scene", path)

func _deferred_goto_scene(path):
#	# It is now safe to remove the current scene
	current_scene.free()
#	# Load the new scene.
	var s = ResourceLoader.load(path)
#	# Instance the new scene.
	current_scene = s.instantiate()
#	# Add it to the active scene, as child of root.
	get_tree().get_root().add_child(current_scene)
#	# Optionally, to make it compatible with the SceneTree.change_scene() API.
	get_tree().set_current_scene(current_scene)

func save_settings():
	#var f = File.new()
	var f = FileAccess.open(settings_file, FileAccess.WRITE)
	#f.open(settings_file, File.WRITE)
	f.store_var(story_mode)
	f.store_var(bg_num)
	f.close()

func load_settings():
	if FileAccess.file_exists(settings_file):
	#var f = File.new()
	#if f.file_exists(settings_file):
		#f.open(settings_file, File.READ)
		var f = FileAccess.open(settings_file, FileAccess.READ)		
		story_mode = f.get_var()
		bg_num = f.get_var()
		f.close()
		
func background_load_scene(path):
	# raise this scene so its visible (or will be)
	# when the progressbar is set to visible in a sec
	#raise()
	move_to_front()
	# create a loader, give it the scene path to load
	backgroundloader = ResourceLoader.load_threaded_request(path)
	# check if the loader is actually loaded
	if backgroundloader == null:
		print("No backgroundloader created!")
		return
	# get the amount of stuff to load and set that as the max for the
	# progress bar
	pb.set_max(backgroundloader.get_stage_count())
	# turn on processing (ie activate the _process(time) function below)
	set_process(true)
	
	# set wait_frames to 1 so the _process will skip a frame before starting
	wait_frames = 1
	# make the progress bar visible
	pb.visible = true
	
func update_progress_bar():
	# get the current loading step, divide it 
	# by the total amount of loading to do,
	# that will be the value passed to the progress bar
	var progress = float(backgroundloader.get_stage() / backgroundloader.get_stage_count())
	
	# set that as the value for the progress bar
	pb.set_value(progress)

func swap_to_backgound_loaded_scene(scene_resource):
	# set the current_scene to disappear
	current_scene.queue_free()
	# set the new scene to be the new current scene and 
	# attach it to the tree
	current_scene = scene_resource.instantiate()
	get_node("/root").add_child(current_scene)
	
	
# ----- built in functions -----

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(time):
	# this function is dormant, until the set_process(true) in 
	# background_load_scene() turns it "on"
	
	# check if we have cleared the resource loader, which will happen at 
	# the end of the function/loop if the resource is done loading
	if backgroundloader == null:
		# no need to process anymore
		set_process(false)
		return
		
	# skip the rest of the loading bits below the first time the
	# _process call is made. set the var to 0 so the next process call
	# will go through
	if wait_frames > 0:
		wait_frames -= 1
		return
		
	# get the current millisecond from the OS
	var t = Time.get_ticks_msec()
	# block the thread for 100 msecs
	while Time.get_ticks_msec() < (t + time_max):
		# run the loader one step, put the result in a var
		var err = backgroundloader.poll()
		
		# if the loader has reached the end of the stuff to load
		# then set it to be null, so the loop will stop the next time
		# the loader is checked (top of this function)
		if err == ERR_FILE_EOF:
			# set the fully loaded resource to a new var
			var resource = backgroundloader.get_resource()
			# set the loader to null
			backgroundloader = null
			# switch to the new scene
			swap_to_backgound_loaded_scene(resource)
			# break the while loop
			break
		# if no error and loader is not finished
		elif err == OK:
			# update the progress bar
			update_progress_bar()
		# break if there is an error
		else: 
			print("Error during background loading!")
			backgroundloader = null
			break
	
	
# ----- ready -----

func _ready():
	root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() - 1)



# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
