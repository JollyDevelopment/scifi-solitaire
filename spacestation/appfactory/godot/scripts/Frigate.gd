extends Node2D

# note to self: this is meant to be the equivalent to
# the cardfactory's build_a_ship.py
# currently set it up to be a frigate, other ship classes could get 
# their own later.

# ----- variables -----
var ship_name = "The Ship"
var ship_class = "frigate"
var ship_captain
var ship_roster
@onready var Officer = preload("res://scenes/Officers.tscn")
@onready var Crew = preload("res://scenes/Crew.tscn")

# ----- contants -----
var dep_file_path = "res://assets/drydock/fleet/assignments/departments.json"

# ----- functions -----

func recruit_crew():
	# open the departments file, pull the info about the ship's organization by ship size
	var deps = {}
	var dep_file = FileAccess.open(dep_file_path, FileAccess.READ)
	#dep_file.open("res://assets/drydock/fleet/assignments/departments.json", dep_file.READ)
	var dep_text = dep_file.get_as_text()
	# setup the json interface
	var json = JSON.new()
	# try to get the data
	var error = json.parse(dep_text)
	# check if it errored, if not load it and 
	# check if its the right stuff
	if error == OK:
		deps = json.data
	#var test_json_conv = JSON.new()
	#test_json_conv.parse(dep_text)
	#var dep_json_parse = test_json_conv.get_data()
	#deps = dep_json_parse.result
	dep_file.close()
	var org = deps[ship_class]
	
	# for loops to build the roster
	var staff = {}
	var crew = {}
	var dept_roster = {}
	var ships_roster = {}
	for dept in org["dep_names"]:
		# create a dict of staff officers
		for o in range(1, (org["officers_per"]+1)):
			var Off = Officer.instantiate()
			Off.department = dept
			staff[o] = Off
			$Bridge.add_child(Off)
			
		# create a dict of crew. Similar to the staff, but the first crew (1) needs to be
		# a non-comm, then start at '1' and make crew up the the number of crew per department
		var CrwNC = Crew.instantiate()
		CrwNC.non_com = true
		crew = {1: CrwNC}
		$Bridge.add_child(CrwNC)
		for c in range(2, (org["crew_per"]+1)):
			var Crw = Crew.instantiate()
			crew[c] = Crw
			$Bridge.add_child(Crw)
			
		# assign both staff and crew dicts to dept_roster dict
		dept_roster = {"staff": staff, "crew": crew}
		
		# assign dept roster to ship roster
		ships_roster[dept] = dept_roster
	
	return ships_roster


func recruit_captain():
	var sc = Officer.instantiate()
	sc.is_cpt = true
	sc.set_name("ship_captain")
	return sc
	
# Called when the node enters the scene tree for the first time.
func _ready():
	# make the captain
	ship_captain = recruit_captain()
	# print(ship_captain)
	# print(ship_captain.full_name)
	$Bridge.add_child(ship_captain)

	# create the crew roster dict
	ship_roster = recruit_crew()
	
