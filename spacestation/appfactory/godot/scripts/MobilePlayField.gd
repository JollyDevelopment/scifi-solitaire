extends Node2D


# ----- variables -----

#var score = 0

# --- instances 
var draw_deck
var encounters_deck
var in_hand
var encounter_stack_00
var encounter_stack_01
var encounter_stack_02
var encounter_stack_03
var frigate
var frigate_instance
var encountersset
var currently_selected_card
@onready var name_plate = get_node("UI/NamePlate")
@onready var name_plate_label = get_node("UI/NamePlate/CenterContainer/Label")
#onready var score_plate = get_node("UI/ScorePlate")
#onready var score_plate_label = get_node("UI/ScorePlate/CenterContainer/Label")
@onready var progress_bar = get_node("UI/ProgressBar")
@onready var undo_reset_close = get_node("UI/UndoResetClose")
@onready var bg_img = get_node("BackGround/Img")
#@onready var loading_label = get_node("UI/HBC_Loading/VBC/LoadingLabel")
@onready var loading_label = get_node("UI/HBC_Loading/VBC/Loading")

# --- arrays 
var card_array = []
var pending_card_array = []
var initial_draw_deck_cards = []
var initial_encounter_stacks = []
var pending_encounter_stacks_array =[]
var card_graveyard = []

# --- positions
var in_hand_card_zero_pos
var in_hand_card_one_pos
var in_hand_card_two_pos
var es_00_ec_pos
var es_00_oc_pos
var es_00_ciac_pos
var es_00_ccac_pos
var es_01_ec_pos
var es_01_oc_pos
var es_01_ciac_pos
var es_01_ccac_pos
var es_02_ec_pos
var es_02_oc_pos
var es_02_ciac_pos
var es_02_ccac_pos
var es_03_ec_pos
var es_03_oc_pos
var es_03_ciac_pos
var es_03_ccac_pos
var dd_x

# --- constants
const Card = preload("res://scenes/Card.tscn")
const Frigate = preload("res://scenes/Frigate.tscn")
const EncountersSet = preload("res://scenes/EncountersSet.tscn")
const DrawDeck = preload("res://scenes/DrawDeck.tscn")
const EncountersDeck = preload("res://scenes/EncountersDeck.tscn")
const EncounterStack = preload("res://scenes/EncounterStack.tscn")
const InHand = preload("res://scenes/InHand.tscn")
const EncounterPopup = preload("res://scenes/MobileEncounterPopup.tscn")
const YouWinPopup = preload("res://scenes/YouWinPopup.tscn")

# --- dimensions
var base_card_x = 234
var base_card_y = 332
var base_progress_bar_x = 936
var i_x
var i_y
var y_5
var y_8
var y_10
var y_15
var y_13
var y_17
var y_20
var y_21
var y_22
var y_23
var y_24
var y_25
var y_30
var y_40
var y_44
var y_50
var y_56
var y_73
var y_80
var y_90
var y_91
var y_92
var y_93
var y_94
var y_95
var y_96
var x_1
var x_3
var x_5
var x_6
var x_7
var x_8
var x_9
var x_10
var x_12
var x_13
var x_14
var x_15
var x_20
var x_21
var x_22
var x_23
var x_24
var x_25
var x_30
var x_40
var x_50
var x_51
var x_52
var x_53
var x_55
var x_60
var x_65
var x_70
var x_75
var x_85
var dd_scale
var ed_scale
var ih_scale
var es_scale
var gui_scale
var in_hand_card_shift_x
var in_hand_card_shift_y
var ed_y_scaled
var ed_x_scaled


# --- status trackers
var es_00_cleared = true
var es_01_cleared = true
var es_02_cleared = true
var es_03_cleared = true


# ------ my functions -----

func set_dimensions():
	# get window dimensions
	i_x = get_window().get_size().x
	i_y = get_window().get_size().y
	
	# set vertical percents
	y_5 = i_y * .05 # 5%
	y_8 = i_y * .08 # 5%
	y_10 = i_y * .10 # 10%
	y_13 = i_y * .13 # 13%
	y_15 = i_y * .15 # 15%
	y_17 = i_y * .17 # 17%
	y_20 = i_y * .20 # 20%
	y_21 = i_y * .21 # 21%
	y_22 = i_y * .22 # 22%
	y_23 = i_y * .23 # 23%
	y_24 = i_y * .24 # 24%
	y_25 = i_y * .25 # 25%
	y_30 = i_y * .30 # 30%
	y_40 = i_y * .40 # 40%
	y_44 = i_y * .44 # 44%
	y_50 = i_y * .50 # 50%
	y_56 = i_y * .56 # 56%
	y_73 = i_y * .73 # 73%
	y_80 = i_y * .80 # 80%
	y_90 = i_y * .90 # 96%
	y_91 = i_y * .91 # 91%
	y_92 = i_y * .92 # 92%
	y_93 = i_y * .93 # 93%
	y_94 = i_y * .94 # 94%
	y_95 = i_y * .95 # 95%
	y_96 = i_y * .96 # 96%
	
	# set horizontal percents
	x_1 = i_x * .01 # 1%
	x_3 = i_x * .03 # 3%
	x_5 = i_x * .05 # 5%
	x_6 = i_x * .06 # 6%
	x_7 = i_x * .07 # 7%
	x_8 = i_x * .08 # 8%
	x_9 = i_x * .09 # 9%
	x_10 = i_x * .10 # 10%
	x_12 = i_x * .12 # 12%
	x_13 = i_x * .13 # 13%
	x_14 = i_x * .14 # 14%
	x_15 = i_x * .15 # 15%
	x_20 = i_x * .20 # 20%
	x_21 = i_x * .21 # 21%
	x_22 = i_x * .22 # 22%
	x_23 = i_x * .23 # 23%
	x_24 = i_x * .24 # 24%
	x_25 = i_x * .25 # 25%
	x_30 = i_x * .30 # 30%
	x_40 = i_x * .40 # 40%
	x_50 = i_x * .50 # 50%
	x_51 = i_x * .51 # 51%
	x_52 = i_x * .52 # 52%
	x_53 = i_x * .53 # 53%
	x_55 = i_x * .55 # 55%
	x_60 = i_x * .60 # 60%
	x_65 = i_x * .65 # 65%
	x_70 = i_x * .70 # 70%
	x_75 = i_x * .75 # 75%
	x_85 = i_x * .85 # 85%
	
	# set the overall scale for the gui elements
	gui_scale = x_20
	
	# set the shift for the in hand cards
	in_hand_card_shift_x = (base_card_x / 4)
	in_hand_card_shift_y = (base_card_y / 4)

func set_background_img():
	var img_path = str("res://assets/background/full/",Global.bg_num,".png")
	bg_img.set_texture(load(img_path))
	bg_img.position = Global.mobile_bg_position
	Global.set_bg_img_scale(get_window().get_size().y)
	bg_img.scale = Global.mobile_bg_scale
	bg_img.centered = false
	
# --- draw deck 

func add_draw_deck():
	# create the DrawDeck instance
	var dd = DrawDeck.instantiate()
	dd.set_name("DD")
	dd.play_field = self
	$PlayArea/DD.add_child(dd)
	# get the deck instance to be able to adjust the image size
	var ddi = $PlayArea/DD.get_node("DD")
	scale_draw_deck(ddi)
	set_draw_deck_position(ddi, dd_scale)
	return ddi
	
func scale_draw_deck(drawdeckinstance):
	dd_scale = (gui_scale / base_card_x)
	drawdeckinstance.set_dd_scale(Vector2(dd_scale,dd_scale))

func set_draw_deck_position(drawdeckinstance, scale):
	# distance in from the right side
	var dd_x_scaled = (drawdeckinstance.size.x * scale)
	dd_x = (i_x - (dd_x_scaled + x_7))
	drawdeckinstance.position.x = dd_x
	# distance up from the bottom
	var dd_y_scaled = (drawdeckinstance.size.y * scale)
	var dd_y = (i_y - (dd_y_scaled + y_13))
	drawdeckinstance.position.y = dd_y
	

# --- encounters deck 

func add_encounters_deck():
	# create the EncountersDeck instance, add it to the right 
	# containter
	var ed = EncountersDeck.instantiate()
	ed.set_name("ED")
	ed.play_field = self
	$PlayArea/ED.add_child(ed)
	# get the deck instance to be able to adjust the image size
	var edi = $PlayArea/ED.get_node("ED")
	scale_encounters_deck(edi)
	set_encounters_deck_position(edi, ed_scale)
	return edi

func scale_encounters_deck(encountdeckinstance):
	ed_scale = (gui_scale / base_card_x)
	encountdeckinstance.set_ed_scale(Vector2(ed_scale,ed_scale))
	
func set_encounters_deck_position(encountdeckinstance, scale):
	# distance in from the left side
	encountdeckinstance.position.x = x_7
	# distance up from the bottom
	ed_y_scaled = (encountdeckinstance.size.y * scale)
	ed_x_scaled = (encountdeckinstance.size.x * scale)
	var ed_y = (i_y - (ed_y_scaled + y_13))
	encountdeckinstance.position.y = ed_y
	

# --- in_hand base & cards 

func add_in_hand():
	# create the InHand instance, add it to the right 
	# containter
	var ih = InHand.instantiate()
	ih.set_name("IH")
	$PlayArea/IH.add_child(ih)
	# get the instance reference
	var ihi = $PlayArea/IH.get_node("IH")
	scale_in_hand(ihi)
	set_in_hand_position(ihi, ih_scale)
	return ihi

func scale_in_hand(inhandinstance):
	ih_scale = (gui_scale / base_card_x)
	inhandinstance.set_ih_scale(Vector2(ih_scale,ih_scale))
	
func set_in_hand_position(inhandinstance, scale):
	# distance in from the left side
	# set the x position to be halfway across the screen, then move it
	# back to the left by half the length of the instance (after scaling it)
	var x_half = (i_x / 2)
	var ih_x_scaled = (inhandinstance.c0.get_texture().get_width() * scale)
	var ih_x = (x_half - ih_x_scaled)
	inhandinstance.position.x = ih_x
	# distance up from the bottom
	var ih_y_scaled = (inhandinstance.c0.get_texture().get_height() * scale)
	var ih_y = (i_y - (ih_y_scaled + y_30))
	inhandinstance.position.y = ih_y
	get_in_hand_base_card_positions()
	
func get_in_hand_base_card_positions():
	await get_tree().process_frame 
	in_hand_card_zero_pos = in_hand.card_zero_pos
	in_hand_card_one_pos = in_hand.card_one_pos
	in_hand_card_two_pos = in_hand.card_two_pos

func scale_in_hand_cards():
	for card in $PlayArea/Card_00.get_children():
		scale_single_card(card)
	
	for card in $PlayArea/Card_01.get_children():
		scale_single_card(card)
	
	for card in $PlayArea/Card_02.get_children():
		scale_single_card(card)
		
func scale_single_card(cardinstance):
	var ci_scale = (gui_scale / base_card_x)
	cardinstance.scale =  Vector2(ci_scale,ci_scale)

func set_in_hand_card_positions():
	await get_tree().process_frame 
	for card in $PlayArea/Card_00.get_children():
		card.position = in_hand_card_zero_pos
	
	for card in $PlayArea/Card_01.get_children():
		card.position = in_hand_card_one_pos
	
	for card in $PlayArea/Card_02.get_children():
		card.position = in_hand_card_two_pos
		
func set_second_in_hand_card_pickable():
	for c in $PlayArea/Card_01.get_children():
		c.call_deferred("enable_collision_shape")
		c.input_pickable = true

func set_third_in_hand_card_pickable():
	for c in $PlayArea/Card_02.get_children():
		c.call_deferred("enable_collision_shape")
		c.input_pickable = true

func report_cards_in_hand():
	return card_array.size()

# --- encounter stacks 

func add_first_four_encounter_stacks():
	# ES_00
	if es_00_cleared:
		# draw the EncounterStack instance
		var es_00 = encounters_deck.initial_encounter()
		# set the name
		es_00.set_name("ES_00")
		es_00.playfield_instance = self
		es_00.is_in_play = true
		# remove the node from the Encounters holder, 
		# add it to the ES_00 node as a child
		$Encounters.remove_child(es_00)
		$PlayArea/ES_00.add_child(es_00)
		# set the check to be false (not cleared)
		es_00_cleared = false
		# assign the instance reference to the global var
		encounter_stack_00 = $PlayArea/ES_00.get_node("ES_00")
	
	# ES_01
	if es_01_cleared:
		# draw the EncounterStack instance
		var es_01 = encounters_deck.initial_encounter()
		# set the name
		es_01.set_name("ES_01")
		es_01.playfield_instance = self
		es_01.is_in_play = true
		# remove the node from the Encounters holder, 
		# add it to the ES_01 node as a child
		$Encounters.remove_child(es_01)
		$PlayArea/ES_01.add_child(es_01)
		# set the check to be false (not cleared)
		es_01_cleared = false		
		# assign the instance reference to the global var
		encounter_stack_01 = $PlayArea/ES_01.get_node("ES_01")
	
	# ES_02
	if es_02_cleared:
		# draw the EncounterStack instance
		var es_02 = encounters_deck.initial_encounter()
		# set the name
		es_02.set_name("ES_02")
		es_02.playfield_instance = self
		es_02.is_in_play = true
		# remove the node from the Encounters holder, 
		# add it to the ES_02 node as a child
		$Encounters.remove_child(es_02)
		$PlayArea/ES_02.add_child(es_02)
		# set the check to be false (not cleared)
		es_02_cleared = false		
		# assign the instance reference to the global var
		encounter_stack_02 = $PlayArea/ES_02.get_node("ES_02")
	
	# ES_03
	if es_03_cleared:
		# draw the EncounterStack instance
		var es_03 = encounters_deck.initial_encounter()
		# set the name
		es_03.set_name("ES_03")
		es_03.playfield_instance = self
		es_03.is_in_play = true
		# remove the node from the Encounters holder, 
		# add it to the ES_03 node as a child
		$Encounters.remove_child(es_03)
		$PlayArea/ES_03.add_child(es_03)
		# set the check to be false (not cleared)
		es_03_cleared = false				
		# assign the instance reference to the global var
		encounter_stack_03 = $PlayArea/ES_03.get_node("ES_03")

	# scale all the stacks
	scale_all_encounter_stacks()

func scale_all_encounter_stacks():
	for es in $PlayArea/ES_00.get_children():
		scale_encounter_stack(es)
		
	for es in $PlayArea/ES_01.get_children():
		scale_encounter_stack(es)
		
	for es in $PlayArea/ES_02.get_children():
		scale_encounter_stack(es)
		
	for es in $PlayArea/ES_03.get_children():
		scale_encounter_stack(es)

func scale_encounter_stack(encounterstackinstance):
	es_scale = (gui_scale / base_card_x)
	encounterstackinstance.set_es_scale(Vector2(es_scale,es_scale))
	
func set_encounter_stack_positions():
	# get the amount to set the first stack in from the
	# left side of the screen
	# get the size of the scaled card width
	var cw = (base_card_x * (gui_scale / base_card_x))

	# subtract that amount from the viewport width 4 times
	# set the left over width to a var
	var l_o_w = (i_x - (cw * 4))

	# subtract 3% from that
	var low = (l_o_w - x_3)

	# divide that amount in half
	var left_low = (low / 2)
	
	# get the amount to come down from the top of the screen
	var es_y_down = y_17
	
	var es00i
	var es01i
	var es02i
	var es03i
	var es00i_pos = left_low
	var es01i_pos = (left_low + cw + x_1)
	var es02i_pos = (left_low + cw + x_1 + cw + x_1)
	var es03i_pos = (left_low + cw + x_1 + cw + x_1 + cw + x_1)

	# get the first encounterstack instance
	# set its x position to match the left side left over width
	# set its y position as well
	for c in $PlayArea/ES_00.get_children():
		if c.name == "ES_00":
			es00i = c
			es00i.position.x = es00i_pos
			es00i.position.y = es_y_down
	
	# get the second encounterstack instance
	# set it's x to be 1% to the right of the first instance
	# set its y position as well
	for c in $PlayArea/ES_01.get_children():
		if c.name == "ES_01":
			es01i = c
			es01i.position.x = es01i_pos
			es01i.position.y = es_y_down
	
	# get the third encounterstack instance
	# set it's x to be 1% to the right of the second instance
	# set its y position as well
	for c in $PlayArea/ES_02.get_children():
		if c.name == "ES_02":
			es02i = c
			es02i.position.x = es02i_pos
			es02i.position.y = es_y_down
	
	# get the fourth encounterstack instance
	# set it's x to be 1% to the right of the third instance
	# set its y position as well
	for c in $PlayArea/ES_03.get_children():
		if c.name == "ES_03":
			es03i = c
			es03i.position.x = es03i_pos
			es03i.position.y = es_y_down

func get_encounter_stack_base_card_positions():
	await get_tree().process_frame
	# stack 00
	for c in $PlayArea/ES_00.get_children():
		if c.name == "ES_00":
			es_00_ec_pos = encounter_stack_00.ec_pos
			es_00_oc_pos = encounter_stack_00.oc_pos
			es_00_ciac_pos = encounter_stack_00.cia_pos
			es_00_ccac_pos = encounter_stack_00.cca_pos
	# stack 01
	for c in $PlayArea/ES_01.get_children():
		if c.name == "ES_01":
			es_01_ec_pos = encounter_stack_01.ec_pos
			es_01_oc_pos = encounter_stack_01.oc_pos
			es_01_ciac_pos = encounter_stack_01.cia_pos
			es_01_ccac_pos = encounter_stack_01.cca_pos
	# stack 02
	for c in $PlayArea/ES_02.get_children():
		if c.name == "ES_02":
			es_02_ec_pos = encounter_stack_02.ec_pos
			es_02_oc_pos = encounter_stack_02.oc_pos
			es_02_ciac_pos = encounter_stack_02.cia_pos
			es_02_ccac_pos = encounter_stack_02.cca_pos
	# stack 03
	for c in $PlayArea/ES_03.get_children():
		if c.name == "ES_03":
			es_03_ec_pos = encounter_stack_03.ec_pos
			es_03_oc_pos = encounter_stack_03.oc_pos
			es_03_ciac_pos = encounter_stack_03.cia_pos
			es_03_ccac_pos = encounter_stack_03.cca_pos

func check_for_empty_encounter_stacks():
	if es_00_cleared:
		return true
	elif es_01_cleared:
		return true
	elif es_02_cleared:
		return true
	elif es_03_cleared:
		return true
	else:
		return false
	

# --- back-end setup 

func build_ship_and_encounters():
	# create a ship instance and load it
	frigate = Frigate.instantiate()
	frigate.set_name("Frigate")
	$Ship.add_child(frigate)
	frigate_instance = $Ship.get_node("Frigate")
	
	# create the encounters for the game
	encountersset = EncountersSet.instantiate()
	encountersset.set_name("EncountersSet")
	encountersset.roster = frigate_instance.ship_roster
	encountersset.ship_captain = frigate_instance.ship_captain
	$Ship.add_child(encountersset)

func create_crew_cards(ccc_es):
	var ccc_array_01 = []
	var ccc_array_02 = []
	# two loops, one through the departments, then one for the encounters inside the
	# departments
	# in the inner loop create the order, complete_action, and initiate_action cards
	# add them all to an array and return it
	for dep in ccc_es.filled_scenarios_list.keys():
		for enc in ccc_es.filled_scenarios_list[dep].keys():
			# create a new officer Card scene
			# set it's variables
			# add it to the tree, then add the reference to the array
			var o_card = Card.instantiate()
			o_card.department = dep
			o_card.rank = "officer"
			o_card.type = "order"
			o_card.name = str(dep,"_oo")
			$Cards.add_child(o_card)
			var o_c_i = $Cards.get_node(str(dep,"_oo"))
			ccc_array_01.append(o_c_i)
			
			# create a new crew card (initiate_action)
			# set its variables
			# add it to the tree, then add the refernce to the array
			var cia_card = Card.instantiate()
			cia_card.department = str(dep)
			cia_card.rank = "crew"
			cia_card.type = "initiate_action"
			cia_card.name = str(dep,"_cia")
			$Cards.add_child(cia_card)
			var cia_c_i = $Cards.get_node(str(dep,"_cia"))
			ccc_array_01.append(cia_c_i)
			
			# create a new crew card (complete_action)
			# set its variables
			# add it to the tree, then add the refernce to the array
			var cca_card = Card.instantiate()
			cca_card.department = str(dep)
			cca_card.rank = "crew"
			cca_card.type = "complete_action"
			cca_card.name = str(dep,"_cca")
			$Cards.add_child(cca_card)
			var cca_c_i = $Cards.get_node(str(dep,"_cca"))
			ccc_array_01.append(cca_c_i)
			
	
	# randomize the cards
	while ccc_array_01.size() > 0:
		var rng = RandomNumberGenerator.new()
		rng.randomize()
		var randchoice = rng.randi_range(0, (ccc_array_01.size() - 1))
		var card_to_transfer = ccc_array_01[randchoice]
		ccc_array_02.append(card_to_transfer)
		ccc_array_01.remove_at(randchoice)	
	
	return ccc_array_02

func create_encounter_stacks(ces_es):
	var ces_array_01 = []
	var ces_array_02 = []
	var num = 0
	# two loops, one through the departments, then one for the encounters inside the
	# departments
	# the inner loop creates the encounterstack instances and adds the scenarios
	# and department to them, then adds the references to the first array
	for dep in ces_es.filled_scenarios_list.keys():
		for encounter in ces_es.filled_scenarios_list[dep].keys():
			# create the EncounterStack instance
			var es = EncounterStack.instantiate()
			# set it's department
			es.department = dep
			# set it's group
			es.add_to_group(dep)
			# set it's scenario
			es.scenario = ces_es.filled_scenarios_list[dep][encounter]
			# set the name
			es.set_name(str(dep,"_",num))
			# add it to the Encounters holder
			$Encounters.add_child(es)
			# get the reference
			var es_i = $Encounters.get_node(str(dep,"_",num))
			# add it to the array
			ces_array_01.append(es_i)
			# increment the num so all the names are unique
			num += 1
			
	# randomize the array
	while ces_array_01.size() > 0:
		var rng = RandomNumberGenerator.new()
		rng.randomize()
		var randchoice = rng.randi_range(0, (ces_array_01.size() - 1))
		var encounterstack_to_transfer = ces_array_01[randchoice]
		ces_array_02.append(encounterstack_to_transfer)
		ces_array_01.remove_at(randchoice)		
	
	return ces_array_02
	
	
# --- gui 

func set_up_gui_dimensions():
	# set the scale
	var uiscale = (gui_scale / base_card_x)
	var uiscale2 = (x_40 / base_card_x)
#	print("uiscale: ", uiscale)
	var uiscalevec = Vector2(uiscale,uiscale)
	var uiscalevec2 = Vector2(uiscale2,uiscale2)
	var pbscale = (x_21 / base_card_x)
#	print("pbscale: ", pbscale)
	var pbscalevec = Vector2(pbscale, pbscale)
	# set up the name plate
	name_plate.scale = uiscalevec2
	name_plate.position.x = x_7
	name_plate.position.y = y_5
	# set up the undo reset close bar
	undo_reset_close.scale = uiscalevec2
#	undo_reset_close.position.x = dd_x
	undo_reset_close.position.x = x_52
	undo_reset_close.position.y = y_5
	# adjust text size if needed
	name_plate.call_deferred("adjust_cpt_name_to_fit")
	# set up the progressbar
#	progress_bar.rect_scale = uiscalevec
	progress_bar.scale = pbscalevec
	# scaled pb x
#	var s_pb_x = (uiscale * base_progress_bar_x)
	var s_pb_x = (pbscale * base_progress_bar_x)
	# half the scaled x
	var half_s_pb_x = (s_pb_x / 2)
	# pb_x = half the screen, minus half the scaled pb x
	progress_bar.position.x = ((i_x / 2) - half_s_pb_x)
	progress_bar.position.y = y_92
	
func increment_score():
	progress_bar.update_progress_by_one()
	
func set_up_cpt_name():
	var n = frigate_instance.ship_captain.last_name
	name_plate_label.text = str("CPT ", n)
	

# --- game loop 

func discard_cards_in_hand():
	# passs the cards back to the discard pile
	while card_array.size() > 0:
		var c = card_array.pop_front()
		draw_deck.discard_card(c)
	
	# remove the card instances from display
	for ci in $PlayArea/Card_00.get_children():
		ci.free()
	for ci in $PlayArea/Card_01.get_children():
		ci.free()
	for ci in $PlayArea/Card_02.get_children():
		ci.free()
		
func add_card_to_pending(card):
	# add the card to the in pending_card_array
	pending_card_array.append(card)
	
func populate_hand():
	while pending_card_array.size() > 0:
		var c = pending_card_array.pop_front()
		card_array.append(c)
	
	# create a new card, use the card_array card to
	# populate the new card's settings
	if card_array.size() >= 1:
		var c0 = Card.instantiate()
		c0.department = card_array[0].department
		c0.rank = card_array[0].rank
		c0.type = card_array[0].type
		c0.name = "C0"
		c0.playfield_instance = self
		c0.input_pickable = true
		$PlayArea/Card_00.add_child(c0)
		var c0i = $PlayArea/Card_00.get_node("C0")
		scale_single_card(c0i)
	
	if card_array.size() >= 2:
		var c1 = Card.instantiate()
		c1.department = card_array[1].department
		c1.rank = card_array[1].rank
		c1.type = card_array[1].type
		c1.name = "C1"
		c1.playfield_instance = self
		$PlayArea/Card_01.add_child(c1)
		var c1i = $PlayArea/Card_01.get_node("C1")
		scale_single_card(c1i)
	
	if card_array.size() >= 3:
		var c2 = Card.instantiate()
		c2.department = card_array[2].department
		c2.rank = card_array[2].rank
		c2.type = card_array[2].type
		c2.name = "C2"
		c2.playfield_instance = self
		$PlayArea/Card_02.add_child(c2)
		var c2i = $PlayArea/Card_02.get_node("C2")
		scale_single_card(c2i)

	set_in_hand_card_positions()

func add_encounterstack_to_pending(encounterstack):
	pending_encounter_stacks_array.append(encounterstack)
	
func populate_encounters():
	if es_00_cleared:
		# draw the EncounterStack instance
		var es_00 = pending_encounter_stacks_array.pop_front()
		# set the name
		es_00.set_name("ES_00")
		es_00.playfield_instance = self
		es_00.is_in_play = true
		# remove the node from the Encounters holder, 
		# add it to the ES_02 node as a child
		$Encounters.remove_child(es_00)
		$PlayArea/ES_00.add_child(es_00)
		# set the check to be false (not cleared)
		es_00_cleared = false
		# assign the instance reference to the global var
		encounter_stack_00 = $PlayArea/ES_00.get_node("ES_00")
		encounter_stack_00.connect_card_match_signals()
		scale_encounter_stack(encounter_stack_00)
		
	elif es_01_cleared:
		# draw the EncounterStack instance
		var es_01 = pending_encounter_stacks_array.pop_front()
		# set the name
		es_01.set_name("ES_01")
		es_01.playfield_instance = self
		es_01.is_in_play = true
		# remove the node from the Encounters holder, 
		# add it to the ES_02 node as a child
		$Encounters.remove_child(es_01)
		$PlayArea/ES_01.add_child(es_01)
		# set the check to be false (not cleared)
		es_01_cleared = false
		# assign the instance reference to the global var
		encounter_stack_01 = $PlayArea/ES_01.get_node("ES_01")
		encounter_stack_01.connect_card_match_signals()
		scale_encounter_stack(encounter_stack_01)
		
	elif es_02_cleared:
		# draw the EncounterStack instance
		var es_02 = pending_encounter_stacks_array.pop_front()
		# set the name
		es_02.set_name("ES_02")
		es_02.playfield_instance = self
		es_02.is_in_play = true
		# remove the node from the Encounters holder, 
		# add it to the ES_02 node as a child
		$Encounters.remove_child(es_02)
		$PlayArea/ES_02.add_child(es_02)
		# set the check to be false (not cleared)
		es_02_cleared = false
		# assign the instance reference to the global var
		encounter_stack_02 = $PlayArea/ES_02.get_node("ES_02")
		encounter_stack_02.connect_card_match_signals()
		scale_encounter_stack(encounter_stack_02)
		
	elif es_03_cleared:
		# draw the EncounterStack instance
		var es_03 = pending_encounter_stacks_array.pop_front()
		# set the name
		es_03.set_name("ES_03")
		es_03.playfield_instance = self
		es_03.is_in_play = true
		# remove the node from the Encounters holder, 
		# add it to the ES_03 node as a child
		$Encounters.remove_child(es_03)
		$PlayArea/ES_03.add_child(es_03)
		# set the check to be false (not cleared)
		es_03_cleared = false
		# assign the instance reference to the global var
		encounter_stack_03 = $PlayArea/ES_03.get_node("ES_03")
		encounter_stack_03.connect_card_match_signals()
		scale_encounter_stack(encounter_stack_03)
		
	# set the stack locations
	set_encounter_stack_positions()

func clear_currently_selected_card(csc):
	if csc.name == "C0":
#		find_card_match_in_array(csc)
		set_second_in_hand_card_pickable()
		card_graveyard.append(card_array.pop_front())
		csc.visible = false
		csc.queue_free()
	elif csc.name == "C1":
#		find_card_match_in_array(csc)
		set_third_in_hand_card_pickable()
		card_graveyard.append(card_array.pop_front())
		csc.visible = false
		csc.queue_free()
	else:
#		find_card_match_in_array(csc)
		card_graveyard.append(card_array.pop_front())
		csc.visible = false
		csc.queue_free()

func clear_game_board():
	# clear the arrays
	var n_to_clear
	while card_array.size() > 0:
		n_to_clear = card_array.pop_front()
		n_to_clear.queue_free()
	while pending_card_array.size() > 0:
		n_to_clear = pending_card_array.pop_front()
		n_to_clear.queue_free()
	while initial_draw_deck_cards.size() > 0:
		n_to_clear = initial_draw_deck_cards.pop_front()
		n_to_clear.queue_free()
	while initial_encounter_stacks.size() > 0:
		n_to_clear = initial_encounter_stacks.pop_front()
		n_to_clear.queue_free()
	while pending_encounter_stacks_array.size() > 0:
		n_to_clear = pending_encounter_stacks_array.pop_front()
		n_to_clear.queue_free()
	while card_graveyard.size() > 0:
		n_to_clear = card_graveyard.pop_front()
		n_to_clear.queue_free()
		
	# clear the in hand cards
	for c in $PlayArea/Card_00.get_children():
		$PlayArea/Card_00.remove_child(c)
		c.queue_free()
	for c in $PlayArea/Card_01.get_children():
		$PlayArea/Card_01.remove_child(c)
		c.queue_free()
	for c in $PlayArea/Card_02.get_children():
		$PlayArea/Card_02.remove_child(c)
		c.queue_free()
	
	# clear the encounter stacks
	for es in $PlayArea/ES_00.get_children():
		$PlayArea/ES_00.remove_child(es)
		es.queue_free()
	for es in $PlayArea/ES_01.get_children():
		$PlayArea/ES_01.remove_child(es)
		es.queue_free()
	for es in $PlayArea/ES_02.get_children():
		$PlayArea/ES_02.remove_child(es)
		es.queue_free()
	for es in $PlayArea/ES_03.get_children():
		$PlayArea/ES_03.remove_child(es)
		es.queue_free()
	
	# reset the progress bar
	progress_bar.reset_progress()
	
	# clear the encounters deck of encounterstacks
	encounters_deck.reset_encounters_deck()
	
	# clear the drawdeck of cards
	draw_deck.reset_draw_deck()
	
func new_game():
	# create the ship and the encounters
	build_ship_and_encounters()
	
	# add the draw deck, get the instance reference
	draw_deck = add_draw_deck()
	
	# add the encounters deck, get the instance reference
	encounters_deck = add_encounters_deck()
	
	# add the InHand instance, get the reference
	# set the card positions
	in_hand = add_in_hand()
	set_in_hand_card_positions()
	
	# create the initial draw deck cards
	var esi = $Ship.get_node("EncountersSet")
	initial_draw_deck_cards = create_crew_cards(esi)
	# add the initial crew cards to the draw deck
	draw_deck.add_initial_cards(initial_draw_deck_cards)
	
	# create the initial encounterstacks
	initial_encounter_stacks = create_encounter_stacks(esi)
	# add the initial encounterstacks to the encounters deck
	encounters_deck.add_initial_encounter_stacks(initial_encounter_stacks)

	# add the begining encounter stacks
	add_first_four_encounter_stacks()
	set_encounter_stack_positions()
	get_encounter_stack_base_card_positions()
	
	# set up the ui
	set_up_cpt_name()
	set_up_gui_dimensions()
	undo_reset_close.playfield_instance = self
			
func check_for_win_condition():
	# check if the es cleared vars are true
	# also check if there are any more encounterstacks in the encounterdeck
	# if both are true then the player has won, pop up the congrats
	if encounters_deck.report_remaining_encounters() == 0:
		if es_00_cleared and es_01_cleared and es_02_cleared and es_03_cleared:
			var winpop = YouWinPopup.instantiate()
			$PlayArea/YW_Pop.add_child(winpop)
			winpop.show_you_win_popup()
	
	
# --- signal connected functions

func _ES_00_officer_card_match():
	# turn the es_00 officer card to visible
	for c in $PlayArea/ES_00.get_children():
		if c.is_officer_card_empty():
			c.fill_officer_card()
			# remove the card being dragged from the stack/memory/etc
			clear_currently_selected_card(currently_selected_card)
	
func _ES_01_officer_card_match():
	# turn the es_01 officer card to visible
	for c in $PlayArea/ES_01.get_children():
		if c.is_officer_card_empty():
			c.fill_officer_card()
			# remove the card being dragged from the stack/memory/etc
			clear_currently_selected_card(currently_selected_card)

func _ES_02_officer_card_match():
	# turn the es_02 officer card to visible
	for c in $PlayArea/ES_02.get_children():
		if c.is_officer_card_empty():
			c.fill_officer_card()
			# remove the card being dragged from the stack/memory/etc
			clear_currently_selected_card(currently_selected_card)
	
func _ES_03_officer_card_match():
	# turn the es_03 officer card to visible
	for c in $PlayArea/ES_03.get_children():
		if c.is_officer_card_empty():
			c.fill_officer_card()
			# remove the card being dragged from the stack/memory/etc
			clear_currently_selected_card(currently_selected_card)

func _ES_00_cia_card_match():
	# turn the es_00 cia card to visible
	for c in $PlayArea/ES_00.get_children():
		if c.is_cia_card_empty():
			c.fill_cia_card()
			# remove the card being dragged from the stack/memory/etc
			clear_currently_selected_card(currently_selected_card)

func _ES_01_cia_card_match():
	# turn the es_01 cia card to visible
	for c in $PlayArea/ES_01.get_children():
		if c.is_cia_card_empty():
			c.fill_cia_card()
			# remove the card being dragged from the stack/memory/etc
			clear_currently_selected_card(currently_selected_card)

func _ES_02_cia_card_match():
	# turn the es_02 cia card to visible
	for c in $PlayArea/ES_02.get_children():
		if c.is_cia_card_empty():
			c.fill_cia_card()
			# remove the card being dragged from the stack/memory/etc
			clear_currently_selected_card(currently_selected_card)

func _ES_03_cia_card_match():
	# turn the es_03 cia card to visible
	for c in $PlayArea/ES_03.get_children():
		if c.is_cia_card_empty():
			c.fill_cia_card()
			# remove the card being dragged from the stack/memory/etc
			clear_currently_selected_card(currently_selected_card)
	
func _ES_00_cca_card_match():
	# turn the es_00 cca card to visible
	for c in $PlayArea/ES_00.get_children():
		c.fill_cca_card()
	# remove the card being dragged from the stack/memory/etc
	clear_currently_selected_card(currently_selected_card)
	# if story mode is enabled show the encounter popup
	# then clear the encounter, otherwise just clear
	if Global.story_mode:
		for c in $PlayArea/ES_00.get_children():
			# instance a popup
			var ep = EncounterPopup.instantiate()
			ep.name = "EP_00"
			ep.playfield_instance = self
			# add it to the holder
			$PlayArea/EP_00.add_child(ep)
			# add the encounter dict
			ep.add_encounter_dict(c.return_scenario_dict())
			# show the popup 
			ep.show_encounter_popup()
			# clear the encounter
			es_00_cleared = true
			c.disconnect_card_match_signals()
			c.queue_free()
#			check_for_win_condition()
	else:
		for c in $PlayArea/ES_00.get_children():
			es_00_cleared = true
			c.disconnect_card_match_signals()
			c.queue_free()
			check_for_win_condition()
	# increase the score
	increment_score()
	
func _ES_01_cca_card_match():
	# turn the es_01 cca card to visible
	for c in $PlayArea/ES_01.get_children():
		c.fill_cca_card()
	# remove the card being dragged from the stack/memory/etc
	clear_currently_selected_card(currently_selected_card)
	# if story mode is enabled show the encounter popup
	# then clear the encounter, otherwise just clear
	if Global.story_mode:
		for c in $PlayArea/ES_01.get_children():
			# instance a popup
			var ep = EncounterPopup.instantiate()
			ep.name = "EP_01"
			ep.playfield_instance = self
			# add it to the holder
			$PlayArea/EP_01.add_child(ep)
			# add the encounter dict
			ep.add_encounter_dict(c.return_scenario_dict())
			# show the popup 
			ep.show_encounter_popup()
			# clear the encounter
			es_01_cleared = true
			c.disconnect_card_match_signals()
			c.queue_free()
#			check_for_win_condition()
	else:
		for c in $PlayArea/ES_01.get_children():
			es_01_cleared = true
			c.disconnect_card_match_signals()
			c.queue_free()
			check_for_win_condition()
	# increase the score
	increment_score()
	
func _ES_02_cca_card_match():
	# turn the es_02 cca card to visible
	for c in $PlayArea/ES_02.get_children():
		c.fill_cca_card()
	# remove the card being dragged from the stack/memory/etc
	clear_currently_selected_card(currently_selected_card)
	# if story mode is enabled show the encounter popup
	# then clear the encounter, otherwise just clear
	if Global.story_mode:
		for c in $PlayArea/ES_02.get_children():
			# instance a popup
			var ep = EncounterPopup.instantiate()
			ep.name = "EP_02"
			ep.playfield_instance = self
			# add it to the holder
			$PlayArea/EP_02.add_child(ep)
			# add the encounter dict
			ep.add_encounter_dict(c.return_scenario_dict())
			# show the popup 
			ep.show_encounter_popup()
			# clear the encounter
			es_02_cleared = true
			c.disconnect_card_match_signals()
			c.queue_free()
#			check_for_win_condition()
	else:
		for c in $PlayArea/ES_02.get_children():
			es_02_cleared = true
			c.disconnect_card_match_signals()
			c.queue_free()
			check_for_win_condition()
	# increase the score
	increment_score()
	
func _ES_03_cca_card_match():
	# turn the es_03 cca card to visible
	for c in $PlayArea/ES_03.get_children():
		c.fill_cca_card()
	# remove the card being dragged from the stack/memory/etc
	clear_currently_selected_card(currently_selected_card)
	# if story mode is enabled show the encounter popup
	# then clear the encounter, otherwise just clear
	if Global.story_mode:
		for c in $PlayArea/ES_03.get_children():
			# instance a popup
			var ep = EncounterPopup.instantiate()
			ep.name = "EP_03"
			ep.playfield_instance = self
			# add it to the holder
			$PlayArea/EP_03.add_child(ep)
			# add the encounter dict
			ep.add_encounter_dict(c.return_scenario_dict())
			# show the popup 
			ep.show_encounter_popup()
			# clear the encounter
			es_03_cleared = true
			c.disconnect_card_match_signals()
			c.queue_free()
#			check_for_win_condition()
	else:
		for c in $PlayArea/ES_03.get_children():
			es_03_cleared = true
			c.disconnect_card_match_signals()
			c.queue_free()
			check_for_win_condition()
	# increase the score
	increment_score()
	
func _Card_00_card_selected():
	currently_selected_card = $PlayArea/Card_00.get_node("C0")
	
func _Card_01_card_selected():
	currently_selected_card = $PlayArea/Card_01.get_node("C1")

func _Card_02_card_selected():
	currently_selected_card = $PlayArea/Card_02.get_node("C2")
	
func _UI_undo_last_action():
	pass
	
func _UI_reset_game():
	# wait a beat, show the loading text
	# wait a beat again, then go load the new
	# scene. If there is no wait things are so fast
	# the loading text is not shown
	await get_tree().process_frame
	loading_label.set_visible(true)
	await get_tree().process_frame
	Global.background_load_scene("res://scenes/MobilePlayField.tscn")
	
func _UI_close_game():
	Global.goto_scene("res://scenes/MobileLandingPage.tscn")


# ----- builtin functions -----

func _on_viewport_size_changed():
	# update the dimensions
	set_dimensions()
	
	# draw deck
	scale_draw_deck(draw_deck)
	set_draw_deck_position(draw_deck, dd_scale)
	
	# encounters deck
	scale_encounters_deck(encounters_deck)
	set_encounters_deck_position(encounters_deck, ed_scale)
	
	# in_hand base
	scale_in_hand(in_hand)
	set_in_hand_position(in_hand, ih_scale)
	get_in_hand_base_card_positions()
	
	# cards in_hand
	set_in_hand_card_positions()
	scale_in_hand_cards()
	
	# encounterstacks
	scale_all_encounter_stacks()
	set_encounter_stack_positions()
	get_encounter_stack_base_card_positions()
	
	set_up_gui_dimensions()
	
#func _input(event):
#	if Input.is_action_pressed("debug_keys"):
##		draw_deck.report_cards()
#		encounters_deck.report_encounters()
##		draw_deck.poll_cards()
##		encounters_deck.poll_encounters()
#		var cardcount = 0
#		if $PlayArea/Card_00.get_node("C0") != null:
#			cardcount += 1
#		if $PlayArea/Card_01.get_node("C1") != null:
#			cardcount += 1
#		if $PlayArea/Card_02.get_node("C2") != null:
#			cardcount += 1			
#		print("cards in hand: ", cardcount)
#		var ddc =  draw_deck.report_cards()
#		print("draw deck cards: ", ddc)
#		print("total: ", (cardcount + ddc))
#		print("card_array: ", card_array)
	
	
# ----- ready ------

# Called when the node enters the scene tree for the first time.
func _ready():
	# set this nodes name
	set_name("PlayField")
	
	# set the x and y dimensions
	set_dimensions()
	
	# set the background
	set_background_img()
	
	# get the viewport size changed signal and 
	# connect the local size_changed function
	get_tree().root.connect("size_changed", Callable(self, "_on_viewport_size_changed"))

	# start the game
	new_game()
	
