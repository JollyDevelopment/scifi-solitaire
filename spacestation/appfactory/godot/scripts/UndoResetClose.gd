extends Sprite2D

# ----- variables -----
var undo_mouse = false
var reset_mouse = false
var close_mouse = false

# ----- instances
var playfield_instance

# ----- signals -----
signal undo
signal reset
signal close



# ----- my functions -----

func connect_signals():
	connect_undo_signal()
	connect_reset_signal()
	connect_close_signal()
	
func connect_undo_signal():
	# create strings to match the playfield functions
	var my_parent_name = get_parent().name
	var playfield_undo_function = str("_",my_parent_name,"_undo_last_action")
	# connect this signal to the playfield function
	if playfield_instance:
		if playfield_instance.has_method(playfield_undo_function):
			connect("undo", Callable(playfield_instance, playfield_undo_function))
	
func connect_reset_signal():
	# create strings to match the playfield functions
	var my_parent_name = get_parent().name
	var playfield_reset_function = str("_",my_parent_name,"_reset_game")
	# connect this signal to the playfield function
	if playfield_instance:
		if playfield_instance.has_method(playfield_reset_function):
			connect("reset", Callable(playfield_instance, playfield_reset_function))
	
func connect_close_signal():
	# create strings to match the playfield encounterstack functions
	var my_parent_name = get_parent().name
	var playfield_close_function = str("_",my_parent_name,"_close_game")
	# connect this signal to the playfield function
	if playfield_instance:
		if playfield_instance.has_method(playfield_close_function):
			connect("close", Callable(playfield_instance, playfield_close_function))

# ----- built in functions -----

# --- UNDO
func _on_Undo_mouse_entered():
	undo_mouse = true

func _on_Undo_mouse_exited():
	undo_mouse = false

func _on_Undo_input_event(viewport, event, shape_idx):
	if Input.is_action_pressed("leftclick") and undo_mouse:
		emit_signal("undo")
		self.get_viewport().set_input_as_handled()
	
# --- RESET
func _on_Reset_mouse_entered():
	reset_mouse = true

func _on_Reset_mouse_exited():
	reset_mouse = false

func _on_Reset_input_event(viewport, event, shape_idx):
	if Input.is_action_pressed("leftclick") and reset_mouse:
		emit_signal("reset")
		self.get_viewport().set_input_as_handled()
	if event is InputEventScreenTouch:
		if event.is_pressed():
			emit_signal("reset")
		self.get_viewport().set_input_as_handled()
	
# --- CLOSE
func _on_Close_mouse_entered():
	close_mouse = true

func _on_Close_mouse_exited():
	close_mouse = false

func _on_Close_input_event(viewport, event, shape_idx):
	if Input.is_action_pressed("leftclick") and close_mouse:
		emit_signal("close")
		self.get_viewport().set_input_as_handled()
	if event is InputEventScreenTouch:
		if event.is_pressed():
			emit_signal("close")
			self.get_viewport().set_input_as_handled()
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

# ----- ready -----


# Called when the node enters the scene tree for the first time.
func _ready():
	call_deferred("connect_signals")
	









