extends Node2D

# ----- variables -----
var department = "command"
var ccs = Global.card_color_set
#var ccs = "default"
var scenario = {}
var ec_pos
var oc_pos
var cia_pos
var cca_pos
var officer_card_empty = true
var cia_card_empty = true
var cca_card_empty = true
var officer_card_filled = false
var cia_card_filled = false
var officer_card_instance
var cia_card_instance
var cca_card_instance
var playfield_instance
var mouse_in = false
var is_in_play = false
signal officer_card_match
signal cia_card_match
signal cca_card_match
@onready var encounter_card_img_path = str("res://assets/cards/",department,"/encounters/",ccs,"/scenario.png")
@onready var officer_card_img_path = str("res://assets/cards/",department,"/officer/",ccs,"/order.png")
@onready var cia_card_img_path = str("res://assets/cards/",department,"/crew/",ccs,"/initiate_action.png")
@onready var cca_card_img_path = str("res://assets/cards/",department,"/crew/",ccs,"/complete_action.png")
@onready var ec_img = get_node("EncounterCard/Img")


# ----- my functions -----

func set_es_scale(vec):
	scale = vec
	
func set_card_imgs():
#	Global.load_settings()
#	print(Global.card_color_set)
#	var ccs = Global.card_color_set
#	var encounter_card_img_path = str("res://assets/cards/",department,"/encounters/",ccs,"/scenario.png")
#	var officer_card_img_path = str("res://assets/cards/",department,"/officer/",ccs,"/order.png")
#	var cia_card_img_path = str("res://assets/cards/",department,"/crew/",ccs,"/initiate_action.png")
#	var cca_card_img_path = str("res://assets/cards/",department,"/crew/",ccs,"/complete_action.png")
	$EncounterCard/Img.set_texture(load(encounter_card_img_path))
	$OfficerCard.set_texture(load(officer_card_img_path))
	$CIACard.set_texture(load(cia_card_img_path))
	$CCACard.set_texture(load(cca_card_img_path))
	
func shift_the_cards():
	# get the current height of the encounter card's img
	var ecy = ec_img.get_texture().get_height()

	# how much to shift the cards down
	var ecy_18 = (ecy * .18) # 18%
	var ecy_36 = (ecy * .36) # 36%
	var ecy_54 = (ecy * .54) # 54%
	
	# get the encounter card img position and use that to adjust the other two cards
	var ecpos = ec_img.position

	# move officer card
	var oc = get_node("OfficerCard")
	var ocnewpos = Vector2(0, (oc.position.y + ecy_18))
	oc.position = ocnewpos

	# move cia card
	var ciac = get_node("CIACard")
	var ciacnewpos = Vector2(0, (ciac.position.y + ecy_36))
	ciac.position = ciacnewpos

	# move the cca card
	var ccac = get_node("CCACard")
	var ccacnewpos = Vector2(0, (ccac.position.y + ecy_54))
	ccac.position = ccacnewpos
	
	# get the global positions of the cards, set them to vars
	# so the playfield can use them to add cards to
	ec_pos = ec_img.get_global_position()
	oc_pos = oc.get_global_position()
	cia_pos = ciac.get_global_position()
	cca_pos = ccac.get_global_position()

func connect_card_match_signals():
	# create strings to match the playfield encounterstack functions
	var self_name_playfield_ocm_function = str("_",self.name,"_officer_card_match")
	var self_name_playfield_cia_cm_function = str("_",self.name,"_cia_card_match")
	var self_name_playfield_cca_cm_function = str("_",self.name,"_cca_card_match")
	# connect this instances card match signals to the playfield functions
	if playfield_instance and is_in_play:
		if playfield_instance.has_method(self_name_playfield_ocm_function):
			connect("officer_card_match", Callable(playfield_instance, self_name_playfield_ocm_function))
		if playfield_instance.has_method(self_name_playfield_cia_cm_function):
			connect("cia_card_match", Callable(playfield_instance, self_name_playfield_cia_cm_function))
		if playfield_instance.has_method(self_name_playfield_cca_cm_function):
			connect("cca_card_match", Callable(playfield_instance, self_name_playfield_cca_cm_function))

func disconnect_card_match_signals():
# create strings to match the playfield encounterstack functions
	var self_name_playfield_ocm_function = str("_",self.name,"_officer_card_match")
	var self_name_playfield_cia_cm_function = str("_",self.name,"_cia_card_match")
	var self_name_playfield_cca_cm_function = str("_",self.name,"_cca_card_match")
	# connect this instances card match signals to the playfield functions
	if playfield_instance and is_in_play:
		if playfield_instance.has_method(self_name_playfield_ocm_function):
			disconnect("officer_card_match", Callable(playfield_instance, self_name_playfield_ocm_function))
		if playfield_instance.has_method(self_name_playfield_cia_cm_function):
			disconnect("cia_card_match", Callable(playfield_instance, self_name_playfield_cia_cm_function))
		if playfield_instance.has_method(self_name_playfield_cca_cm_function):
			disconnect("cca_card_match", Callable(playfield_instance, self_name_playfield_cca_cm_function))
	
func is_officer_card_empty():
	return officer_card_empty
	
func is_cia_card_empty():
	return cia_card_empty

func fill_officer_card():
	# called by the playfield intance on this stack when 
	# the officer_card_match signal goes off
	$OfficerCard.set_visible(true)
	officer_card_empty = false
	officer_card_filled = true
	
func fill_cia_card():
	$CIACard.set_visible(true)
	cia_card_empty = false
	cia_card_filled = true
	
func fill_cca_card():
	$CCACard.set_visible(true)

func return_scenario_dict():
	return scenario
	

# ----- built in functions -----

func _on_EncounterCard_area_entered(area):
	if mouse_in:
		if area.is_in_group(department):
			if area.is_in_group("officer") and officer_card_empty:
				emit_signal("officer_card_match")
			elif area.is_in_group("initiate_action") and officer_card_filled and cia_card_empty:
				emit_signal("cia_card_match")
			elif area.is_in_group("complete_action") and officer_card_filled and cia_card_filled:
				emit_signal("cca_card_match")
			
func _on_EncounterCard_mouse_entered():
	mouse_in = true

func _on_EncounterCard_mouse_exited():
#	print("mouse out")
	mouse_in = false
	

# ----- ready -----

# Called when the node enters the scene tree for the first time.
func _ready():
	set_card_imgs()
	call_deferred("shift_the_cards")
	call_deferred("connect_card_match_signals")

	









#func _on_EncounterCard_area_entered(area):
#	print(area.get_parent().name, " ", area.name)


#func _on_EncounterCard_input_event(viewport, event, shape_idx):
#	if Input.is_ # Replace with function body.


#func _on_EncounterCard_input_event(viewport, event, shape_idx):
#	if event is InputEventMouseMotion:
#		mouse_in = true # Replace with function body.
