extends Node2D


# ---- variables ------
@onready var progress_bar = get_node("UI/ProgressBar")

# --- dimensions
var i_x
var i_y
var x_5
var x_10
var x_15
var x_20
var x_80
var x_80_10
var y_1
var y_2
var y_5
var y_10
var y_15
var y_20
var y_50


# ----- my functions -----

func set_dimensions():
	# get window dimensions
	i_x = get_window().get_size().x
	i_y = get_window().get_size().y

	# set vertical percents
	y_1 = i_y * .01 # 1%
	y_2 = i_y * .02 # 2%
	y_5 = i_y * .05 # 5%
	y_10 = i_y * .10 # 10%
	y_15 = i_y * .15 # 15%
	y_20 = i_y * .20 # 20%
	y_50 = i_y * .50 # 50%
	
	# set horizontal percents
	x_5 = i_x * .05 # 5%
	x_10 = i_x * .10 # 10%
	x_15 = i_x * .15 # 15%
	x_20 = i_x * .20 # 20%
	x_80 = i_x * .80 # 20%
	
	# get increments for ship movement
	x_80_10 = (x_80 / 10)
	
func setup_progress_bar():
	progress_bar.position.x = x_10
	progress_bar.position.y = (y_50 - y_1)
	progress_bar.size.y = y_2
	progress_bar.size.x = x_80
	
func update_progress(num):
	progress_bar.set_value(num)
	
func set_bar_max(num):
	progress_bar.set_max(num)


# ----- built in functions -----

#func _on_Timer_timeout():
#	update_progress()
	
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

# ----- ready -----

# Called when the node enters the scene tree for the first time.
func _ready():
	set_dimensions()
	setup_progress_bar()
	
	






