extends MarginContainer


# ----- variables -----
@onready var bg_img = get_node("Background/Img")


# ----- my functions -----


# ----- built in functions ----


# Called when the node enters the scene tree for the first time.
func _ready():
	# load the right os page
	if Global.current_os == "Android":
		Global.background_load_scene("res://scenes/MobileLandingPage.tscn")
	else:
		Global.background_load_scene("res://scenes/LandingPage.tscn")
	
