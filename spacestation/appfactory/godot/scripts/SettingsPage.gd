extends MarginContainer


# ---- variables -----
var i_x
var i_y
var back_mouse_in = false
var backgound_mouse_in = false
var card_theme_mouse_in = false
@onready var story_mode_button = get_node("UI/HBC/VBC/HBC/CheckButton")
@onready var bg_img = get_node("Background/Img")


# ----- my functions -----

func set_dimensions():
	# get window dimensions
	i_x = get_window().get_size().x
	i_y = get_window().get_size().y
	
func set_background_img():
	var img_path = str("res://assets/background/full/",Global.bg_num,".png")
	bg_img.set_texture(load(img_path))
	bg_img.set_position(Global.bg_position)
	Global.set_bg_img_scale(i_y)
	bg_img.set_scale(Global.bg_scale)
	bg_img.set_centered(false)

func set_story_mode_toggle_state():
	if Global.story_mode:
		story_mode_button.set_pressed(true)
	else:
		story_mode_button.set_pressed(false)
	
	
# ----- built in functions


# --- story mode

func _on_StoryMode_CheckButton_mouse_entered():
	pass # Replace with function body.

func _on_StoryMode_CheckButton_mouse_exited():
	pass # Replace with function body.

func _on_StoryMode_CheckButton_toggled(_button_pressed):
	Global.story_mode = story_mode_button.button_pressed
	Global.save_settings()
	
# --- Background

func _on_Background_Img_mouse_entered():
	backgound_mouse_in = true

func _on_Background_Img_mouse_exited():
	backgound_mouse_in = false

func _on_Background_Img_gui_input(event):
	if Input.is_action_just_pressed("leftclick") and backgound_mouse_in:
		Global.goto_scene("res://scenes/BackgroundSelector.tscn")
	if event is InputEventScreenTouch:
		if event.is_pressed():
			Global.goto_scene("res://scenes/BackgroundSelector.tscn")
	
# --- Card Theme


func _on_CardTheme_Img_mouse_entered():
	card_theme_mouse_in = true
	
func _on_CardTheme_Img_mouse_exited():
	card_theme_mouse_in = false
	
func _on_CardTheme_Img_gui_input(event):
	if Input.is_action_just_pressed("leftclick") and card_theme_mouse_in:
		Global.goto_scene("res://scenes/CardThemeSelector.tscn")
	if event is InputEventScreenTouch:
		if event.is_pressed():
			Global.goto_scene("res://scenes/CardThemeSelector.tscn")
	
	
# --- Back button

func _on_Back_Img_mouse_entered():
	back_mouse_in = true

func _on_Back_Img_mouse_exited():
	back_mouse_in = false

func _on_Back_Img_gui_input(event):
	if Input.is_action_just_pressed("leftclick") and back_mouse_in:
		Global.goto_scene("res://scenes/LandingPage.tscn")
	if event is InputEventScreenTouch:
		if event.is_pressed():
			Global.goto_scene("res://scenes/LandingPage.tscn")
	

# ----- built in functions -----

func _on_viewport_size_changed():
	# update the dimensions
	set_dimensions()
	set_background_img()
	
# ----- ready -----

# Called when the node enters the scene tree for the first time.
func _ready():
	# load the settings
	Global.load_settings()
	set_dimensions()
	set_background_img()
	set_story_mode_toggle_state()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass














