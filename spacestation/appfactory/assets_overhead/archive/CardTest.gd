extends KinematicBody2D


# ----- variables -----
var department = "support"
var rank = "crew"
var type = "complete_action"
onready var card_img_path = str("res://assets/cards/",department,"/",rank,"/",type,".png")
var playfield_instance
var mouse_in = false
var dragging = false
var speed = 100
#var global_mouse_position_while_dragging
#var mouse_to_center
var mouse_pos
#var xdiff
#var ydiff
var movement
var cs_signal_sent = false
#var able_to_be_picked = false
#signal dragsignal
signal currently_selected

# ----- my functions -----

func set_img_texture():
	$Img.set_texture(load(card_img_path))

#func _set_dragging_state():
#	dragging=!dragging
	
func add_to_groups():
	add_to_group(department)
	add_to_group(rank)
	add_to_group(type)


func connect_currently_selected_signal():
	# create strings to match the playfield encounterstack functions
	var my_parent_name = get_parent().name
	var playfield_currently_selected_card_function = str("_",my_parent_name,"_card_selected")
	# connect this instances card match signals to the playfield functions
	if playfield_instance:
		if playfield_instance.has_method(playfield_currently_selected_card_function):
#			print("connecting currently_selected signal")
			connect("currently_selected",playfield_instance,playfield_currently_selected_card_function)

	
	
# ----- builtin functions -----

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	if dragging:
#		mouse_pos = get_viewport().get_mouse_position()
#		movement = mouse_pos - position;
#		move_and_collide(movement)

func _physics_process(delta):
	if dragging:
#		mouse_pos = get_viewport().get_mouse_position()
		mouse_pos = get_global_mouse_position()
		movement = mouse_pos - position;
#		if movement.length() > (speed * delta):
#			movement = speed * delta * movement.normalized()
		move_and_collide(movement)
#		move_and_slide(movement)

func _on_Card_input_event(viewport, event, shape_idx):
	if Input.is_action_pressed("leftclick"):
		dragging = true
		if not cs_signal_sent:
			emit_signal("currently_selected")
			cs_signal_sent = true
		self.get_tree().set_input_as_handled()
		$Img.centered = true
		print("global: ", get_global_mouse_position())
		print("local: ", get_local_mouse_position())
		print("viewport: ", get_viewport().get_mouse_position())

func _unhandled_input(event):
	# have to catch the released button in an unhandled event, not sure why 
	# yet
	if Input.is_action_just_released("leftclick"):
		# turn off dragging
		dragging = false
		# reset the currently_selected_sent check
		cs_signal_sent = false
		# fix the collisionshape discrepency, resulting from changing the 
		# img.centered to true
		$CollShape.position = $Img.position


# ----- ready -----

# Called when the node enters the scene tree for the first time.
func _ready():
	set_img_texture()
	add_to_groups()
#	connect("dragsignal",self,"_set_dragging_state")
	call_deferred("connect_currently_selected_signal")
	








