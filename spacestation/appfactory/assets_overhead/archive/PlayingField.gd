extends MarginContainer

# ----- variables -----
var draw_deck
var encounters_deck
var in_hand
var frigate
var encountersset
var initial_draw_deck_cards = []
var in_hand_need_shift = true

# constants
const Card = preload("res://scenes/Card.tscn")
const CardTest = preload("res://scenes/CardTest.tscn")
const Frigate = preload("res://scenes/Frigate.tscn")
const EncountersSet = preload("res://scenes/EncountersSet.tscn")
const DrawDeck = preload("res://scenes/DrawDeck.tscn")
const DiscardDeck = preload("res://scenes/DiscardDeck.tscn")
const EncountersDeck = preload("res://scenes/EncountersDeck.tscn")
const InHand = preload("res://scenes/InHand.tscn")

# dimensions
var i_x
var i_y
var y_10
var y_30
var y_50
var x_10
var x_15
var x_20
var x_25
var x_30


# ----- my functions -----

func set_container_dimensions():
	# get window dimensions
	i_x = OS.get_window_size().x
	i_y = OS.get_window_size().y
	
	# set vertical percents
	y_10 = i_y * .10 # 10%
	y_50 = i_y * .50 # 50%
	y_30 = i_y * .30 # 30%
	
	# set horizontal percents
	x_10 = i_x * .10 # 10%
	x_15 = i_x * .15 # 15%
	x_20 = i_x * .20 # 20%
	x_25 = i_x * .25 # 25%
	x_30 = i_x * .30 # 30%
#	print(x_30)
	
	# set hbscorename height
	$VBoxTop/HBScoreName.rect_min_size.y = y_10
	# set hbprogressbar height
	$VBoxTop/HBProgressBar.rect_min_size.y = y_10
	# set hbencounters min height
	$VBoxTop/HBEncounters.rect_min_size.y = y_50
	# set hbdecks min height
	$VBoxTop/HBDecks.rect_min_size.y = y_30
	
	# set EncountersDeckCont min width
	$VBoxTop/HBDecks/EncountersDeckCont.rect_min_size.x = x_15
	# set the DrawDeckCont min width
	$VBoxTop/HBDecks/DrawDeckCont.rect_min_size.x = x_15
	# set Score min width
	$VBoxTop/HBScoreName/Score.rect_min_size.x = x_30
	# set Name min width
	$VBoxTop/HBScoreName/Name.rect_min_size.x = x_30
	

func add_draw_deck():
	# create the DrawDeck instance, add it to the right 
	# containter
	var dd = DrawDeck.instance()
	dd.set_name("DD")
	$VBoxTop/HBDecks/DrawDeckCont.add_child(dd)
	# get the deck instance to be able to adjust the image size
	var ddi = $VBoxTop/HBDecks/DrawDeckCont.get_node("DD")
#	print("added draw deck" + ddi.name)
#	scale_draw_deck_img(ddi)
	return ddi
	
	
func scale_draw_deck_img(drawdeckinstance):	
	# get the container size, get the percent to scale the deck image
#	var ddc_x = $VBoxTop/HBDecks/DrawDeckCont.rect_min_size.x
#	var ddc_y = $VBoxTop/HBDecks/DrawDeckCont.rect_min_size.y
#	print(ddc_y)
	var current_dd_y = drawdeckinstance.get_texture().get_size().y
#	print(current_dd_y)
	var new_scale = (y_30 / current_dd_y)
#	print(new_scale)
#	print('ddi = ')
#	print(drawdeckinstance)
	drawdeckinstance.set_dd_scale(Vector2(new_scale,new_scale))


func add_encounters_deck():
	# create the EncountersDeck instance, add it to the right 
	# containter
	var ed = EncountersDeck.instance()
	ed.set_name("ED")
	$VBoxTop/HBDecks/EncountersDeckCont.add_child(ed)
	# get the deck instance to be able to adjust the image size
	var edi = $VBoxTop/HBDecks/EncountersDeckCont.get_node("ED")
#	print(edi)
#	scale_encounters_deck_img(edi)
#	set_encounters_deck_position(edi)
	return edi
	
	
func scale_encounters_deck_img(encountersdeckinstance):	
	# get the container size, get the percent to scale the deck image
	var edc_x = $VBoxTop/HBDecks/EncountersDeckCont.rect_size.x
	var edc_y = $VBoxTop/HBDecks/EncountersDeckCont.rect_size.y
	var current_ed_y = encountersdeckinstance.get_texture().get_size().y
#	print(current_dd_y)
	var new_scale = (edc_y / current_ed_y)
#	print(new_scale)
	encountersdeckinstance.set_ed_scale(Vector2(new_scale,new_scale))
	
	
func set_encounters_deck_position(encountersdeckinstance):
	# get the container x size
	var edc_x = $VBoxTop/HBDecks/EncountersDeckCont.rect_size.x
#	print(edc_x)
	# get the ed current texture size
	var current_ed_x = encountersdeckinstance.get_texture().get_size().x
#	print(current_ed_x)
	var x_diff = (edc_x - current_ed_x)
#	print(x_diff)
	# set the ed position to move it x_diff to the right
	encountersdeckinstance.translate(Vector2(-x_diff,0))
	
	
func add_in_hand():
	# create the InHand instance, add it to the right 
	# containter
	var ih = InHand.instance()
	ih.set_name("IH")
	$VBoxTop/HBDecks/InHandCont.add_child(ih)
	# get the instance reference
	var ihi = $VBoxTop/HBDecks/InHandCont.get_node("IH")
#	yield(get_tree(), "idle_frame") 
#	ihi.shift_the_cards()
	return ihi
	
	
func connect_deck_and_hand():
	# give the drawdeck the inhand instance reference
	draw_deck.in_hand_instance = in_hand
	# give the inhand the drawdeck instance
	in_hand.draw_deck_instance = draw_deck
	
	
func create_crew_cards(ccc_es):
	var ccc_array_01 = []
	var ccc_array_02 = []
	# two loops, one through the departments, then one for the encounterrs inside the
	# departments
	# in the inner loop create the order, complete_action, and initiate_action cards
	# add them all to an array and return it
	for dep in ccc_es.filled_scenarios_list.keys():
#		print(dep)
		for enc in ccc_es.filled_scenarios_list[dep].keys():
#			print(dep)
#			print(ccc_es.filled_scenarios_list[dep][enc]["Title"])
			
			# create a new officer Card scene
			# set it's variables
			# add it to the tree, then add the refernce to the array
			var o_card = Card.instance()
			o_card.department = dep
			o_card.rank = "officer"
			o_card.type = "order"
			o_card.name = str(dep,"_oo")
			$Cards.add_child(o_card)
			var o_c_i = $Cards.get_node(str(dep,"_oo"))
			ccc_array_01.append(o_c_i)
			
			# create a new crew card (initiate_action)
			# set its variables
			# add it to the tree, then add the refernce to the array
			var cia_card = Card.instance()
			cia_card.department = dep
			cia_card.rank = "crew"
			cia_card.type = "initiate_action"
			cia_card.name = str(dep,"_cia")
			$Cards.add_child(cia_card)
			var cia_c_i = $Cards.get_node(str(dep,"_cia"))
			ccc_array_01.append(cia_c_i)
			
			# create a new crew card (complete_action)
			# set its variables
			# add it to the tree, then add the refernce to the array
			var cca_card = Card.instance()
			cca_card.department = dep
			cca_card.rank = "crew"
			cca_card.type = "complete_action"
			cca_card.name = str(dep,"_cca")
			$Cards.add_child(cca_card)
			var cca_c_i = $Cards.get_node(str(dep,"_cca"))
			ccc_array_01.append(cca_c_i)
			
	
	# randomize the cards
	while ccc_array_01.size() > 0:
		print("01 before trans: ",ccc_array_01.size())
#		print("02 before trans: ",ccc_array_02.size())
		var rng = RandomNumberGenerator.new()
		rng.randomize()
		var randchoice = rng.randi_range(0, (ccc_array_01.size() - 1))
		var card_to_transfer = ccc_array_01[randchoice]
		ccc_array_02.append(card_to_transfer)
		ccc_array_01.remove(randchoice)
#		print("01 after trans: ",ccc_array_01.size())
		print("02 after trans: ",ccc_array_02.size())		
	
	return ccc_array_02
	
	
func update_score_dimensions():
	var win_x = OS.get_window_size().x
	var win_y = OS.get_window_size().y
	
	# get x 30%, apply to score and name boxes
	var x_thirty_percent = win_x * .30
	$VBoxTop/HBoxScoreName/Score/test_score.rect_min_size.x = int(x_thirty_percent)
	$VBoxTop/HBoxScoreName/Name/test_name.rect_min_size.x = int(x_thirty_percent)
	
	# get y 30%, apply to draw and discard
	var y_thirty_percent = win_y * .30
	$VBoxTop/HBoxContainer4/DiscardPileCont/test_discardC.rect_min_size.y = int(y_thirty_percent)
	$VBoxTop/HBoxContainer4/DiscardPileCont/test_discardC.rect_min_size.y = int(y_thirty_percent)
	
	# getx 25%, apply to draw and discard
	$VBoxTop/HBoxContainer4/DiscardPileCont/test_discardC.rect_min_size.y = int(y_thirty_percent)


func build_ship_and_encounters():
	# create a ship instance and load it
	frigate = Frigate.instance()
	frigate.set_name("Frigate")
	$Ship.add_child(frigate)
	var fi = $Ship.get_node("Frigate")
#	print(fi.ship_captain.full_name)
	
	# create the encounters for the game
	encountersset = EncountersSet.instance()
	encountersset.set_name("EncountersSet")
	encountersset.roster = fi.ship_roster
	encountersset.ship_captain = fi.ship_captain
#	print(e.roster['command']['staff'][1].rank)
	$Ship.add_child(encountersset)


func test_card():
	var tc = CardTest.instance()
	tc.department = "command"
	tc.rank = "crew"
	tc.type = "initiate_action"
	add_child(tc)
	
# ----- builtin functions -----

# use this to make the vboxes/hboxes dynamically change size
func _on_viewport_size_changed():
	# set the containers dimensions
	set_container_dimensions()
	# shift the inhand cards
	in_hand.shift_the_cards()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	shift_monitor()


# ----- ready -----

# Called when the node enters the scene tree for the first time.
func _ready():
	# set the containers dimensions
	set_container_dimensions()
	
	# create the ship and the encounters
#	print("building ship and encouinters")
	build_ship_and_encounters()
	
	# get the viewport size changed signal and 
	# connect the local size_changed function
	get_tree().root.connect("size_changed", self, "_on_viewport_size_changed")
	
	# add the draw deck, get the instance reference
#	print("adding draw deck")
	draw_deck = add_draw_deck()
#	print(draw_deck.name)
	
	# add the encounters deck, get the instance reference
#	print("adding encounters deck")
	encounters_deck = add_encounters_deck()
	
	# add the InHand instance, get the reference
	in_hand = add_in_hand()
	
	# tell the draw deck and the inhand instances about each otherr
	connect_deck_and_hand()
	
	# create the initial draw deck cards
	var esi = $Ship.get_node("EncountersSet")
	initial_draw_deck_cards = create_crew_cards(esi)
#	print(initial_draw_deck_cards)

	# add the initial crew cards to the draw deck
	draw_deck.add_initial_cards(initial_draw_deck_cards)
	
	test_card()
	


