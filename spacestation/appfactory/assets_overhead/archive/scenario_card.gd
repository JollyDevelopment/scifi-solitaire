extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var screen_size
var department = ["infra", "support", "command"]
var target = Vector2()

export var speed = 400


# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	screen_size = get_viewport_rect().size
	var sz = department.size()
	$AnimatedSprite.animation = department[randi() % sz]
	target = $AnimatedSprite.position
	
#func _input(event):
#	if event is InputEventScreenTouch and event.pressed:
#		target = event.position


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#    pass
#func _process(delta):
#	var velocity = Vector2()
#	# Move towards the target and stop when close.
#	if position.distance_to(target) > 10:
#		velocity = (target - position).normalized() * speed
#	else:
#		velocity = Vector2()
#		
#	position += velocity * delta
	# We still need to clamp the player's position here because on devices that don't
	# match your game's aspect ratio, Godot will try to maintain it as much as possible
	# by creating black borders, if necessary.
	# Without clamp(), the player would be able to move under those borders.
#	position.x = clamp(position.x, 0, screen_size.x)
#	position.y = clamp(position.y, 0, screen_size.y)
