extends MarginContainer


# ----- variables -----
onready var progress = get_node("UI/HBC/VBC/LoadProgress")
var x = 0

# ----- my functions ------

func update_progress(amount):
	progress.set_value(amount)

# ----- built in functions -----

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

# ----- ready -----


# Called when the node enters the scene tree for the first time.
func _ready():
#	update_progress(42)
	pass



