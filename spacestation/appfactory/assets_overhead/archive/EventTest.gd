extends Area2D




# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	var new_card = load("res://scenes/Card.tscn").duplicate(true)
#	new_card.set_department("command")
	var nc2 = new_card.instance()
	nc2.set_name("nc2")
	nc2.department = "support"
	add_child(nc2)
	var nci = get_node("nc2")
#	nci.department = "command"


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_EventTest_input_event(viewport, event, shape_idx):
#	if Input.is_action_pressed("leftclick"):
#		print("pressed")
#	elif Input.is_action_just_released("leftclick"):
#		print("released")
	
#	if InputEventMouseButton.pressed:
#		print("true")
#	else:
#		print("false")
	if event is InputEventMouseButton:
		if event.pressed:
			print(event.as_text())
			print("pressed")
		elif not event.pressed:
			print(event.as_text())
			print("released")
