extends Control

# ----- variables -----


# ----- my functions -----

func set_topbuffer_y(size):
	$VBC/TopBuffer.rect_min_size = Vector2(0,size)

func set_leftbuffer_x(size):
	$VBC/HBC/LeftBuffer.rect_min_size = Vector2(size,0)
	
func set_rightbuffer_x(size):
	$VBC/HBC/RightBuffer.rect_min_size = Vector2(size,0)

#func set_namebackground_size(vec):
#	$VBC/HBC/CPT/Background.rect_min_size = vec

func set_namebackground_scale(vec):
#	$VBC/HBC/CPT/Background.rect_scale = vec
	pass
	
func set_scorebackground_x(size):
	$VBC/HBC/Score/Background.rect_min_size.x = size

func set_cpt_name(name):
	var n = str('CPT ',name)
	$VBC/HBC/CPT/Background/Name.text = n
	
func update_score():
	pass

# ----- built in functions -----

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


# ----- ready -----

# Called when the node enters the scene tree for the first time.
func _ready():
#	print($VBC/HBC/LeftBuffer.rect_min_size)
#	print($VBC/HBC/CPT/Background/Name.get_content_height())
#	set_leftbuffer_x(25)
#	set_topbuffer_y(25)
#	set_rightbuffer_x(25)
#	set_cpt_name("Roberts")
#	set_namebackground_scale(Vector2(.5,.5))
#	print($VBC/HBC/CPT/Background.get_texture().rect_scale)
	pass

