extends Panel


# ----- variables -----
var draw_deck_instance
var card_one
var card_two
var card_three
var card_array = []
var pending_card_array = []
const Card = preload("res://scenes/Card.tscn")

onready var c0 = get_node("Card_00") 

# ----- my functions -----

func set_ih_scale(vec):
	rect_scale = vec


func set_panel_size():
	self.rect_size.x = (c0.rect_size.x * 2)
	self.rect_size.y = c0.rect_size.y
	
	
func shift_the_cards():
	# panel x size
	var px = self.rect_size.x
#	print(px)

	# c1 x size
#	var c0 = get_node("Card_00")
	var c0x = c0.rect_size.x
#	print(c0x)

	# how much to shift the cards to the right
	var c1x = (c0x / 2)
	var c2x = (c1x * 2)
#	var c2x = 100
#	var c3x = 200
	
	# add the width of c1 and the shaft amount for c3, then subtract that from
	# the panel dimensions, then divide the result by 2
	var shift = (px - (c0x + c2x)) / 2
#	print(shift)
	
	# move the c1 card to the right by the shift amount
	c0.rect_position = Vector2((self.rect_position.x + shift),0)
	
	# get the new c1 position and use that to adjust the other two cards
	var c0pos = c0.rect_position
	# move card 2
	var c1 = get_node("Card_01")
	var c1newpos = Vector2((c0.rect_position.x + c1x), 0)
	c1.rect_position = c1newpos
	# move card 3
	var c2 = get_node("Card_02")
	var c2newpos = Vector2((c0.rect_position.x + c2x), 0)
	c2.rect_position = c2newpos


func discard_cards_in_hand():
	# passs the cards back to the discard pile
	while card_array.size() > 0:
		var c = card_array.pop_front()
		print("discarding ",c)
		draw_deck_instance.discard_card(c)
	
	# remove the card instances from display
	for ci in $Card_00.get_children():
		ci.free()
	for ci in $Card_01.get_children():
		ci.free()
	for ci in $Card_02.get_children():
		ci.free()
	
	
func add_card_to_pending(card):
	# add the card to the in pending_card_array
#	print(pending_card_array)
	pending_card_array.append(card)
	
	
func populate_hand():
	while pending_card_array.size() > 0:
		var c = pending_card_array.pop_front()
		print("adding ",c," to card_array")
		card_array.append(c)
#	print("final array: ",card_array)
	
	# create a new card, use the card_array card to
	# populate the new card's settings
	var c0 = Card.instance()
	c0.department = card_array[0].department
	c0.rank = card_array[0].rank
	c0.type = card_array[0].type
	$Card_00.add_child(c0)
	
	var c1 = Card.instance()
	c1.department = card_array[1].department
	c1.rank = card_array[1].rank
	c1.type = card_array[1].type
	$Card_01.add_child(c1)
	
	var c2 = Card.instance()
	c2.department = card_array[2].department
	c2.rank = card_array[2].rank
	c2.type = card_array[2].type
	$Card_02.add_child(c2)
	
	print("final array: ",card_array)


# ----- built in functions -----



# ----- ready -----


# Called when the node enters the scene tree for the first time.
func _ready():
#	set_panel_size()
	call_deferred("shift_the_cards")
	
#	test_card_instance()
#	print("ready - cards shifted!")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
#	call_deferred("set_panel_size")
	call_deferred("shift_the_cards")
