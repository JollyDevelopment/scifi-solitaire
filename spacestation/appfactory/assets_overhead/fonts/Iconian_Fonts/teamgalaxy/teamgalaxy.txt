Team Galaxy Font

2020 Iconian Fonts - Daniel Zadorozny 

http://www.iconian.com/

This font comes with the following 25 versions: Regular, Italic, Semi-Italic, Super-Italic, Leftalic, Sem-Leftalic, Condensed, Condensed Italic, Condensed Super-Italic, Expanded, Expanded Italic, Expanded Super-Italic, 3D, 3D Italic, Gradient, Gradient Italic, Gradient 2, Gradient 2 Italic, Haltone, Halftone Italic, Twotone, Twotone Italic, Laser, Laser Italic and Laser Super-Italic. 

This font may be freely distributed and is free for all non-commercial uses.  Use of the fonts are at your own risk. 

For commercial use of the font please visit http://iconian.com/commercial.html for additional information.

or e-mail me at iconian@aol.com  
