# SciFi-Solitaire

I love sci-fi, and spaceships and speculative fiction in general. 
I also like playing games on my Android phone. The one game I find that I play the most is Solitaire. 
This is mostly because the only time I really have to play is on subway commutes. So i've been trying to find a good sci-fi
card game that's as easy to pick-up and put down as solitaire. Unfortunately after a couple years of trying various games none 
of them really scratched that itch perfectly. So I am making one. 

This repo contains the basis for the guts of the game. I want the cards to be dynamically generated when each game is played,
so each time you play it will be a different story. That's for the digital version. I also want to have a way to create physical cards,
so that you can create a deck and print it on your own if that's your inclination.


* The [Rules](Rules_Of_Engagement.md) describe how to play.
* The [Spacestation](spacestation) contains the code that generates the cards used to play.
* The [App Factory](spacestation/appfactory) holds the code I use to actually make digital apps to play. 
* The [Card Factory](spacestation/cardfactory) holds the code to create printable card designs

## How to Contribute

* [Write scenarios!](overhead/scenarios/) Adding scenarios to the plot will expand the playability of the game. Having more scenarios means each time the game is played it will be a unique experience.

* Write Code! I'm self taught so I'm always open to learning new and better ways to make the 1's and 0's dance. Make sure your contributions are well commented is all I ask.

* Art Work! I'm more concentrated on the code at the moment, so the art is going to be of the "place-holder" variety for now. If you're interested in upgrading please do so! You must be ok with the [license](License.md) though.


## Required setup for development environment

This is the setup(s) I have on my machines. 

### MacOS

* Required Software
  * [git](https://git-scm.com/download/mac)
  * Python2.7, Python3.7 (Came with MacOS Mojave)
  * virtualenv 
    * `sudo pip install virtualenv`
* Optional Software
  * ipython (installed in the virtual environment)
  * [pptx](https://python-pptx.readthedocs.io/en/latest/index.html) (in venv, used for printing cards)
  * [LibrerOffice](https://www.libreoffice.org/) (in venv, used for printing cards)
  * [Pycharm](https://www.jetbrains.com/pycharm/download/#section=mac) - Community Edition
  
### Linux (Arch)

* Required Software
  * [git](https://git-scm.com/download/mac)
  * Python2.7, Python3.8
  * virtualenv 
    * `sudo pip install virtualenv`
* Optional Software
  * ipython (installed in the virtual environment)
  * [pptx](https://python-pptx.readthedocs.io/en/latest/index.html) (in venv, used for printing cards)
  * [LibrerOffice](https://www.libreoffice.org/) (in venv, used for printing cards)
  * [Pycharm](https://www.jetbrains.com/pycharm/download/#section=mac) - Community Edition

## Planned Milestones

* 0.0
  * <s>repo setup</s>
  * <s>milestones listed</s>
  * <s>original notes dispersed</s>

* 0.1 
  * <s>Write enough scenario sets for a small ship game (10)</s>
  * <s>Create place holder artwork</s>
    * <s>Department Icons: Command, Support, Infrastructure</s>
    * <s>Officer Icon</s>
    * <s>Crew Icon</s>
    * <s>Initiate Action Icon</s>
    * <s>Complete Action Icon</s>
  * <s>Write the initial rules set</s>
  * <s>Write the intro readme.md</s>
  * <s>Decide on a License</s>
    * <s>For the code</s>
    * <s>For the writing/media</s>
  * <s>Write the gameplay description</s>

* 0.5
  * <s>Initial Public opening of the repo.</s>
  * <s>Godot project setup</s>
  * <s>Create scripts to generate the cards</s>

* 0.9
  * <s>Write enough scenario sets for three small ships</s>
  * <s>Finalize solo rules set</s>
  * <s>Finalize the name</s>

* 1.0
  * <s>1.0 artwork</s>
  * <s>1.0 Linux app release</s>
  * <s>1.0 MacOS app release</s>
  * <s>1.0 Windows app release</s>