# scifi-solitaire

Scenarios are the 'main' gameplay/obstacle. Each ship/deck size will be generated with a set number of scenarios.

I'd like there to be a lot more scenarios sets than there are needed for each ship size. That way with a large enough pool of scenario sets each game can be unique.

Scenarios should be written to [Scenarios_List.odt](Scenarios_List.odt). There is a template at the top of the document.  
Scenarios should also be written to [scenarios.json](../../spacestation/expeditions/plot/scenarios.json).
