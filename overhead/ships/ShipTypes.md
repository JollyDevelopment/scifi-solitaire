# scifi-solitaire

Ships come in three size classes: Small, Medium, and Large.

Crew Organization is split into departments, with larger ships having more departments as the size increases.

* Small = 3
* Medium = 5
* Large = 8

Size equals difficulty.
Difficulty equals the number of cards in a combo to clear a Scenario:

* Small -> 1 officer order, 2 crew action
* Medium -> 2 officer order, 4 crew action
* Large -> 3 officer order, 6 crew action

Small Ship Size: Frigates ()
Medium Ship Size: Combat Cruiser, Merchant Cruiser
Large Ship Size: Battleship (Combat), Freighter (Merchant)

Frigates have combined combat/merchant capabilities.
Medium and Large ships have dedicated combat or merchant versions.

Combat ships will encounter more combat oriented Scenarios.
Merchant ships can/will still encounter combat Scenarios, but much less frequently.

