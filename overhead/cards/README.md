# scifi-solitaire

There will be three types of cards for the solo/solitaire style game.

* Scenario Cards
* Officer Order Cards
* Crew Initiate Action Cards
* Crew Complete Action Cards

Thematically there will be departments that these cards are associated with.

The cards will be organized as scenario sets.

-each department has one officer and three crew***
- each officer job has 6 command cards***
-each crew member has two action cards***
-each department has twelve cards total***
